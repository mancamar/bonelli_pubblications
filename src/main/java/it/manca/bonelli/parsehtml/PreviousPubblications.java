package it.manca.bonelli.parsehtml;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author marcomanca
 */
public class PreviousPubblications {
    private final String pubblicationDateKey = "uscita:";
    private final String pubblicationNumberKey = "N° :";
    private final String pubblicationPeriodicityKey = "Periodicità:";

    private String siteUrl;
    private Document doc;
    private String baseUrl = "http://www.sergiobonelli.it";

    public PreviousPubblications(String siteUrl) {
        this.siteUrl = siteUrl;
        try {
            this.doc = Jsoup.connect(this.siteUrl).get();
        } catch (IOException ex) {
            Logger.getLogger(PreviousPubblications.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public void setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
    }

    public Document getDoc() {
        return doc;
    }

    public void setDoc(Document doc) {
        this.doc = doc;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
    
    

    
    
}
