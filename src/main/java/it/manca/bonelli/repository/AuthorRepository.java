package it.manca.bonelli.repository;

import it.manca.bonelli.model.Author;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Marco Manca
 */
@Repository
public interface AuthorRepository extends JpaRepository<Author, Long>{  
    List<Author> findAll();
    List<Author> findAuthorsBySurname(String surname);
    List<Author> findAuthorByNameSurname(String nameSurname);
    List<Author> findAuthorByNameAndSurname(String name, String surname);
}
