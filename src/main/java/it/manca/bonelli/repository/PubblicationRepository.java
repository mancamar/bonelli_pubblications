package it.manca.bonelli.repository;

import it.manca.bonelli.model.Pubblication;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author marcomanca
 */
@Repository
public interface PubblicationRepository extends JpaRepository<Pubblication, Long>{  
    List<Pubblication> findAll();
    
    List<Pubblication> findDistinctPubblicationsByPubblicationName(String name);
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p " +
        "where p.pubblicationName= ?1 " +        
        "order by p.pubblicationDate DESC " +
        "limit 1",
            nativeQuery = true)  
    Pubblication findLastPubblicationByPubblicationName(String name);
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p " +
        "where p.pubblicationName= ?1 " +        
        "order by p.number ASC " +
        "limit ?2,1",
            nativeQuery = true)  
    Pubblication findPubblicationByOrdinalPubblicationName(String name,int ordinal);
            
    List<Pubblication> findPubblicationsByPubblicationNameAndPubblicationDateBetween(String name, Date startDate, Date endDate);
    
    /* START COUNT */
    @Query(value = "select COUNT(distinct p.pubblicationName, p.number) " +
        "from pubblication p, pubblication_author pa1, author a " +
        "where a.surname = ?1 and " +
        "pa1.authorId=a.authorId and pa1.pubblicationName=p.pubblicationName and pa1.number=p.number",
            nativeQuery = true)    
    int countDistinctPubblicationsByPubblicationAuthors_Pk_Author_Surname(String surname);
    
    @Query(value = "select COUNT(distinct p.pubblicationName, p.number) " +
        "from pubblication p, pubblication_author pa1, author a " +
        "where a.surname = ?3 and " +
        "p.pubblicationDate>=?1 and p.pubblicationDate<=?2 and " +    
        "pa1.authorId=a.authorId and pa1.pubblicationName=p.pubblicationName and pa1.number=p.number",
            nativeQuery = true)    
    int countDistinctPubblicationsByPubblicationDateBetweenAndPubblicationAuthors_Pk_Author_Surname(Date startDate, Date endDate, String surname);
    
    @Query(value = "select COUNT(distinct p.pubblicationName, p.number) " +
        "from pubblication p, pubblication_author pa1, author a " +
        "where a.nameSurname = ?1 and " +        
        "pa1.authorId=a.authorId and pa1.pubblicationName=p.pubblicationName and pa1.number=p.number",
            nativeQuery = true)    
    int countDistinctPubblicationsByPubblicationAuthors_Pk_Author_NameSurname(String nameSurname);
    
    @Query(value = "select COUNT(distinct p.pubblicationName, p.number) " +
        "from pubblication p, pubblication_author pa1, author a " +
        "where a.nameSurname = ?3 and " +
        "p.pubblicationDate>=?1 and p.pubblicationDate<=?2 and " +    
        "pa1.authorId=a.authorId and pa1.pubblicationName=p.pubblicationName and pa1.number=p.number",
            nativeQuery = true)
    int countDistinctPubblicationsByPubblicationDateBetweenAndPubblicationAuthors_Pk_Author_NameSurname(Date startDate, Date endDate, String nameSurname);
    
    @Query(value = "select COUNT(distinct p.pubblicationName, p.number) " +
        "from pubblication p, pubblication_author pa1, author a " +
        "where a.surname = ?2 and " +
        "p.pubblicationName=?1 and " +    
        "pa1.authorId=a.authorId and pa1.pubblicationName=p.pubblicationName and pa1.number=p.number",
            nativeQuery = true)    
    int countDistinctPubblicationsByPubblicationNameAndPubblicationAuthors_Pk_Author_Surname(String comic_name, String surname);
    
    @Query(value = "select COUNT(distinct p.pubblicationName, p.number) " +
        "from pubblication p, pubblication_author pa1, author a " +
        "where a.surname = ?4 and " +
        "p.pubblicationDate>=?2 and p.pubblicationDate<=?3 and " +    
        "p.pubblicationName=?1 and " +    
        "pa1.authorId=a.authorId and pa1.pubblicationName=p.pubblicationName and pa1.number=p.number",
            nativeQuery = true)    
    int countDistinctPubblicationsByPubblicationNameAndPubblicationDateBetweenAndPubblicationAuthors_Pk_Author_Surname(String comic_name, 
            Date startDate, Date endDate, String surname);
    
    @Query(value = "select COUNT(distinct p.pubblicationName, p.number) " +
        "from pubblication p, pubblication_author pa1, author a " +
        "where a.nameSurname = ?2 and " +        
        "p.pubblicationName=?1 and " +    
        "pa1.authorId=a.authorId and pa1.pubblicationName=p.pubblicationName and pa1.number=p.number",
            nativeQuery = true)    
    int countDistinctPubblicationsByPubblicationNameAndPubblicationAuthors_Pk_Author_NameSurname(String comic_name, String nameSurname);
    
    @Query(value = "select COUNT(distinct p.pubblicationName, p.number) " +
        "from pubblication p, pubblication_author pa1, author a " +
        "where a.nameSurname = ?4 and " +
        "p.pubblicationDate>=?2 and p.pubblicationDate<=?3 and " +    
        "p.pubblicationName=?1 and " +    
        "pa1.authorId=a.authorId and pa1.pubblicationName=p.pubblicationName and pa1.number=p.number",
            nativeQuery = true)    
    int countDistinctPubblicationsByPubblicationNameAndPubblicationDateBetweenAndPubblicationAuthors_Pk_Author_NameSurname(String comic_name, 
            Date startDate, Date endDate, String nameSurname);
    /* END COUNT */
    
    /* START IN NEWSSTAND */
    List<Pubblication> findDistinctPubblicationsByCurrentInNewsstandOrderByPubblicationDateAsc(boolean isInNewstand);    
    List<Pubblication> findDistinctPubblicationsByCurrentInNewsstandAndPubblicationName(boolean isInNewstand, String comicName);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.name = ?2 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number and p.currentInNewsstand=?1 " +
        "group by p.number, p.pubblicationName "+
        "order by p.pubblicationDate ASC ",
            nativeQuery = true)    
    List<Pubblication> findDistinctPubblicationsByCurrentInNewsstandAndPubblicationAuthors_Pk_Author_Name(boolean isInNewstand,String name);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.surname = ?2 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number and p.currentInNewsstand=?1 " +
        "group by p.number, p.pubblicationName "+
        "order by p.pubblicationDate ASC ",
            nativeQuery = true)    
    List<Pubblication> findDistinctPubblicationsByCurrentInNewsstandAndPubblicationAuthors_Pk_Author_Surname(boolean isInNewstand, String surname);        
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.nameSurname = ?2 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number and p.currentInNewsstand=?1 " +
        "group by p.number, p.pubblicationName "+
        "order by p.pubblicationDate ASC ",
            nativeQuery = true)    
    List<Pubblication> findDistinctPubblicationsByCurrentInNewsstandAndPubblicationAuthors_Pk_Author_NameSurname(boolean isInNewstand, String nameSurname);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.surname = ?3 and p.pubblicationName= ?2 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number and p.currentInNewsstand=?1 " +
        "group by p.number, p.pubblicationName "+
        "order by p.pubblicationDate ASC ",
            nativeQuery = true)  
    List<Pubblication> findDistinctPubblicationsByCurrentInNewsstandAndPubblicationNameAndPubblicationAuthors_Pk_Author_Surname(boolean isInNewstand, String comic_name, String surname);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.nameSurname = ?3 and p.pubblicationName= ?2 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number and p.currentInNewsstand=?1 " +
        "group by p.number, p.pubblicationName "+
        "order by p.pubblicationDate ASC ",
            nativeQuery = true)  
    List<Pubblication> findDistinctPubblicationsByCurrentInNewsstandAndPubblicationNameAndPubblicationAuthors_Pk_Author_NameSurname(boolean isInNewstand, String comic_name, String nameSurname);
    /* END IN NEWSSTAND */

    /* START NEXT */
    List<Pubblication> findDistinctPubblicationsByPubblicationDateAfterOrderByPubblicationDateAsc(Date date);
    //List<Pubblication> findDistinctPubblicationsByPubblicationDateAfterAndCurrentInNewsstand(Date date, boolean inNewsstand);
    List<Pubblication> findDistinctPubblicationsByPubblicationDateAfterAndPubblicationName(Date date, String comicName);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.name = ?2 and p.pubblicationDate>=?1 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName "+
        "order by p.pubblicationDate ASC ",
            nativeQuery = true)  
    List<Pubblication> findDistinctPubblicationsByPubblicationDateAfterAndPubblicationAuthors_Pk_Author_Name(Date date, String name);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.surname = ?2 and p.pubblicationDate>=?1 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName "+
        "order by p.pubblicationDate ASC ",
            nativeQuery = true)  
    List<Pubblication> findDistinctPubblicationsByPubblicationDateAfterAndPubblicationAuthors_Pk_Author_Surname(Date date, String surname);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.nameSurname = ?2 and p.pubblicationDate>=?1 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName"+
        "order by p.pubblicationDate ASC ",
            nativeQuery = true)  
    List<Pubblication> findDistinctPubblicationsByPubblicationDateAfterAndPubblicationAuthors_Pk_Author_NameSurname(Date date, String nameSurname);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.surname = ?3 and p.pubblicationDate>=?1 and p.pubblicationName = ?2 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName "+
        "order by p.pubblicationDate ASC ",
            nativeQuery = true) 
    List<Pubblication> findDistinctPubblicationsByPubblicationDateAfterAndPubblicationNameAndPubblicationAuthors_Pk_Author_Surname(Date date, String comicName, String surname);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.nameSurname = ?3 and p.pubblicationDate>=?1 and p.pubblicationName = ?2 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName "+
        "order by p.pubblicationDate ASC ",
            nativeQuery = true) 
    List<Pubblication> findDistinctPubblicationsByPubblicationDateAfterAndPubblicationNameAndPubblicationAuthors_Pk_Author_NameSurname(Date date, String comicName, String nameSurname);    
    
    List<Pubblication> findDistinctPubblicationsByPubblicationDateBetweenOrderByPubblicationDateAsc(Date startDate, Date endDate);
    /* END NEXT */
    
    /* START SEARCH BY YEAR */    
    List<Pubblication> findDistinctPubblicationsByPubblicationDateBetweenAndPubblicationName(Date startDate, Date endDate, String pubName);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.surname = ?3 and p.pubblicationDate>=?1 and p.pubblicationDate<=?2 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName "+
        "order by p.pubblicationDate ASC ",
            nativeQuery = true) 
    List<Pubblication> findDistinctPubblicationsByPubblicationDateBetweenAndPubblicationAuthors_Pk_Author_Surname(Date startDate, Date endDate, String surname);    
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.nameSurname = ?3 and p.pubblicationDate>=?1 and p.pubblicationDate<=?2 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName "+
        "order by p.pubblicationDate ASC ",
            nativeQuery = true) 
    List<Pubblication> findDistinctPubblicationsByPubblicationDateBetweenAndPubblicationAuthors_Pk_Author_NameSurname(Date startDate, Date endDate, String nameSurname);    
    /* END SEARCH BY YEAR */
    
    /* START COMIC NAME AND NUMBER  */
    Pubblication findPubblicationByPubblicationNameAndNumber(String name, int number);
    /* END COMIC NAME AND NUMBER  */
    
    Pubblication findPubblicationByPubblicationNameAndPubblicationTitle(String name, String title);
    
    /* START AUTHOR  */
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.name = ?1 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName "+
        "order by p.pubblicationDate ASC ",
            nativeQuery = true) 
    List<Pubblication> findDistinctPubblicationsByPubblicationAuthors_Pk_Author_Name(String name);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.surname = ?1 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName "+
        "order by p.pubblicationDate ASC ",
            nativeQuery = true) 
    List<Pubblication> findDistinctPubblicationsByPubblicationAuthors_Pk_Author_Surname(String surname);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.nameSurname = ?1 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName "+
        "order by p.pubblicationDate ASC ",
            nativeQuery = true) 
    List<Pubblication> findDistinctPubblicationsByPubblicationAuthors_Pk_Author_NameSurname(String nameSurname);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.surname = ?2 and p.pubblicationName= ?1 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName "+
        "order by p.pubblicationDate ASC ",
            nativeQuery = true)  
    List<Pubblication> findDistinctPubblicationsByPubblicationNameAndPubblicationAuthors_Pk_Author_Surname(String pubName, String surname);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.nameSurname = ?2 and p.pubblicationName= ?1 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName "+
        "order by p.pubblicationDate ASC ",
            nativeQuery = true)  
    List<Pubblication> findDistinctPubblicationsByPubblicationNameAndPubblicationAuthors_Pk_Author_NameSurname(String pubName, String namesurname);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.surname = ?2 and p.pubblicationName= ?1 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName " +
        "order by p.pubblicationDate DESC " +
        "limit 1",
            nativeQuery = true)  
    Pubblication findLastPubblicationByPubblicationNameAndPubblicationAuthors_Pk_Author_Surname(String pubName, String surname);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.surname = ?3 and p.pubblicationName= ?2 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName " +
        "order by p.number ASC " +
        "limit ?1,1",
            nativeQuery = true)  
    Pubblication findPubblicationByOrdinalAndPubblicationNameAndPubblicationAuthors_Pk_Author_Surname(int ordinal, String pubName, String surname);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.nameSurname = ?2 and p.pubblicationName= ?1 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName "+ 
        "order by p.pubblicationDate DESC " +
        "limit 1",
            nativeQuery = true)  
    Pubblication findLastPubblicationByPubblicationNameAndPubblicationAuthors_Pk_Author_NameSurname(String pubName, String namesurname);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.nameSurname = ?3 and p.pubblicationName= ?2 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName "+ 
        "order by p.number ASC " +
        "limit ?1, 1",
            nativeQuery = true)  
    Pubblication findPubblicationByOrdinalAndPubblicationNameAndPubblicationAuthors_Pk_Author_NameSurname(int ordinal, String pubName, String namesurname);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.surname = ?1 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName " +
        "order by p.pubblicationDate DESC " +
        "limit 1",
            nativeQuery = true)  
    Pubblication findLastPubblicationByPubblicationAuthors_Pk_Author_Surname(String surname);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.surname = ?2 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName " +
        "order by p.pubblicationDate ASC " +
        "limit ?1,1",
            nativeQuery = true)  
    Pubblication findPubblicationByOrdinalAndPubblicationAuthors_Pk_Author_Surname(int ordinal, String surname);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.nameSurname = ?1 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName "+ 
        "order by p.pubblicationDate DESC " +
        "limit 1",
            nativeQuery = true)  
    Pubblication findLastPubblicationByPubblicationAuthors_Pk_Author_NameSurname(String namesurname);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.nameSurname = ?2 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName "+ 
        "order by p.pubblicationDate ASC " +
        "limit ?1,1",
            nativeQuery = true)  
    Pubblication findPubblicationByOrdinalAndPubblicationAuthors_Pk_Author_NameSurname(int ordinal, String namesurname);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.surname = ?4 and p.pubblicationDate>=?2 and p.pubblicationDate<=?3 and p.pubblicationName = ?1 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName "+
        "order by p.pubblicationDate ASC ",
            nativeQuery = true) 
    List<Pubblication> findDistinctPubblicationsByPubblicationNameAndPubblicationDateBetweenAndPubblicationAuthors_Pk_Author_Surname(String pubName, Date startDate, Date endDate, String surname);
    
    @Query(value = "select p.number, p.pubblicationName, p.bigCoverURL, p.currentInNewsstand, p.periodicity, p.pubblicationDate, p.pubblicationTitle, p.schedaURL, p.smallCoverURL, p.summary " +
        "from pubblication p, pubblication_author pa, author a " +
        "where a.nameSurname = ?4 and p.pubblicationDate>=?2 and p.pubblicationDate<=?3 and p.pubblicationName = ?1 and pa.authorId=a.authorId and pa.pubblicationName=p.pubblicationName and pa.number=p.number " +
        "group by p.number, p.pubblicationName "+
        "order by p.pubblicationDate ASC ",
            nativeQuery = true) 
    List<Pubblication> findDistinctPubblicationsByPubblicationNameAndPubblicationDateBetweenAndPubblicationAuthors_Pk_Author_NameSurname(String pubName, Date startDate, Date endDate, String nameSurname);
    /* END AUTHOR  */
}
