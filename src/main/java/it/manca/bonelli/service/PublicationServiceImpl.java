package it.manca.bonelli.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.manca.bonelli.model.Pubblication;
import it.manca.bonelli.model.PubblicationAuthor;
import it.manca.bonelli.model.PublicationAuthorId;
import it.manca.bonelli.repository.PubblicationRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import org.hibernate.Hibernate;
import org.springframework.transaction.annotation.Propagation;

@Service("publicationService")
@Transactional
public class PublicationServiceImpl implements PublicationService {

    @Autowired
    private PubblicationRepository pubblicationRepository;

    @Transactional
    public void save(Pubblication pub) {
        pubblicationRepository.save(pub);
    }

    public void delete(Pubblication pub) {
        pubblicationRepository.delete(pub);

    }

    @Transactional
    public void update(Pubblication pub) {
        pubblicationRepository.save(pub);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Pubblication> findAllPublications() {
        List<Pubblication> pubblications = pubblicationRepository.findAll();
//        for (Pubblication p : pubblications) {
//            for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
//                Hibernate.initialize(pa.getAuthor());
//            }
//        }
        return pubblications;
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<Pubblication> findPublicationsByComicName(String name) {
        List<Pubblication> pubblications = pubblicationRepository.findDistinctPubblicationsByPubblicationName(name);
//        if (pubblications != null) {
//            for (Pubblication p : pubblications) {
//                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
//                    Hibernate.initialize(pa.getAuthor());
//                }
//            }
//        }
        return pubblications;
    }
    
    @Override
    @Transactional(readOnly = true)
    public Pubblication findLastPubblicationByComicName(String name) {
        Pubblication pubblication = pubblicationRepository.findLastPubblicationByPubblicationName(name);
        if (pubblication != null) {
            for (PubblicationAuthor pa : pubblication.getPubblicationAuthors()) {
                Hibernate.initialize(pa.getAuthor());
            }
        } else {
            pubblication = new Pubblication();
            pubblication.setNumber(-1);
        }
        return pubblication;
    }
    
    @Override
    @Transactional(readOnly = true)
    public Pubblication findPubblicationByOrdinalAndComicName(int ordinal, String name) {        
        Pubblication pubblication = pubblicationRepository.findPubblicationByOrdinalPubblicationName(name, --ordinal);
        if (pubblication != null) {
            for (PubblicationAuthor pa : pubblication.getPubblicationAuthors()) {
                Hibernate.initialize(pa.getAuthor());
            }
        } else {
            pubblication = new Pubblication();
            pubblication.setNumber(-1);
        }
        return pubblication;
    }
                    
    @Transactional(readOnly = true)
    @Override
    public List<Pubblication> findPublicationsByComicNameAndYear(String name, int year) {
        Date[] dates = getDatesFromYear(year);
        return pubblicationRepository.findPubblicationsByPubblicationNameAndPubblicationDateBetween(name,dates[0], dates[1]);
    }

    @Transactional(readOnly = true)
    @Override
    public int countPublicationsByComicNameAndAuthorSurname(String comic_name, String surname) {
        return pubblicationRepository.countDistinctPubblicationsByPubblicationNameAndPubblicationAuthors_Pk_Author_Surname(comic_name, surname);
    }
    
    @Override
    public int countPublicationsByAuthorSurname(String surname) {
        return pubblicationRepository.countDistinctPubblicationsByPubblicationAuthors_Pk_Author_Surname(surname);
    }

    @Override
    public int countPublicationsByYearAndAuthorSurname(int year, String surname) {
        Date[] dates = getDatesFromYear(year);
        return pubblicationRepository.countDistinctPubblicationsByPubblicationDateBetweenAndPubblicationAuthors_Pk_Author_Surname(dates[0], dates[1], surname);
    }

    @Override
    public int countPublicationsByAuthorNameSurname(String nameSurname) {
        return pubblicationRepository.countDistinctPubblicationsByPubblicationAuthors_Pk_Author_NameSurname(nameSurname);
    }

    @Override
    public int countPublicationsByYearAndAuthorNameSurname(int year, String nameSurname) {
        Date[] dates = getDatesFromYear(year);
        return pubblicationRepository.countDistinctPubblicationsByPubblicationDateBetweenAndPubblicationAuthors_Pk_Author_NameSurname(dates[0], dates[1], nameSurname);
    }

    @Override
    public int countPublicationsByComicNameAndYearAndAuthorSurname(String comic_name, int year, String surname) {
        Date[] dates = getDatesFromYear(year);
        return pubblicationRepository.countDistinctPubblicationsByPubblicationNameAndPubblicationDateBetweenAndPubblicationAuthors_Pk_Author_Surname(comic_name, dates[0], dates[1], surname);
    }

    @Override
    public int countPublicationsByNameAndAuthorNameSurname(String comic_name, String nameSurname) {
        return pubblicationRepository.countDistinctPubblicationsByPubblicationNameAndPubblicationAuthors_Pk_Author_NameSurname(comic_name, nameSurname);
    }

    @Override
    public int countPublicationsByNameAndYearAndAuthorNameSurname(String comic_name, int year, String nameSurname) {
        Date[] dates = getDatesFromYear(year);
        return pubblicationRepository.countDistinctPubblicationsByPubblicationNameAndPubblicationDateBetweenAndPubblicationAuthors_Pk_Author_NameSurname(comic_name, dates[0], dates[1], nameSurname);
    }
    
    @Override
    public List<Pubblication> findPublicationsCurrentInNewsstandByPubblicationName(String comicName) {
        //Hibernate.initialize
        List<Pubblication> pubblications = pubblicationRepository.findDistinctPubblicationsByCurrentInNewsstandAndPubblicationName(true, comicName);
        if (pubblications != null) {
            for (Pubblication p : pubblications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return pubblications;
    }

    @Override
    public List<Pubblication> findPublicationsCurrentInNewsstand() {
        //Hibernate.initialize
        List<Pubblication> pubblications = pubblicationRepository.findDistinctPubblicationsByCurrentInNewsstandOrderByPubblicationDateAsc(true);
        if (pubblications != null) {
            for (Pubblication p : pubblications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return pubblications;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Pubblication> findPublicationsCurrentInNewsstandByAuthorName(String name) {
        //Hibernate.initialize
        List<Pubblication> pubblications
                = pubblicationRepository.findDistinctPubblicationsByCurrentInNewsstandAndPubblicationAuthors_Pk_Author_Name(true, name);        
        if (pubblications != null) {
            for (Pubblication p : pubblications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return pubblications;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Pubblication> findPublicationsCurrentInNewsstandByAuthorSurname(String surname) {
        //Hibernate.initialize
        List<Pubblication> pubblications
                = pubblicationRepository.findDistinctPubblicationsByCurrentInNewsstandAndPubblicationAuthors_Pk_Author_Surname(true, surname);        
        if (pubblications != null) {
            for (Pubblication p : pubblications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return pubblications;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Pubblication> findPublicationsCurrentInNewsstandByAuthorNameSurname(String nameSurname) {
        //Hibernate.initialize
        List<Pubblication> pubblications
                = pubblicationRepository.findDistinctPubblicationsByCurrentInNewsstandAndPubblicationAuthors_Pk_Author_NameSurname(true, nameSurname);
        if (pubblications != null) {
            for (Pubblication p : pubblications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }        
        return pubblications;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Pubblication> findPublicationsCurrentInNewsstandByComicNameAndAuthorSurname(String comic_name, String surname) {
        //Hibernate.initialize
        List<Pubblication> pubblications
                = pubblicationRepository.findDistinctPubblicationsByCurrentInNewsstandAndPubblicationNameAndPubblicationAuthors_Pk_Author_Surname(true, comic_name, surname);        
        if (pubblications != null) {
            for (Pubblication p : pubblications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return pubblications;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Pubblication> findPublicationsCurrentInNewsstandByComicNameAndAuthorNameSurname(String comic_name, String nameSurname) {
        //Hibernate.initialize
        List<Pubblication> pubblications
                = pubblicationRepository.findDistinctPubblicationsByCurrentInNewsstandAndPubblicationNameAndPubblicationAuthors_Pk_Author_NameSurname(true, comic_name, nameSurname);        
        if (pubblications != null) {
            for (Pubblication p : pubblications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return pubblications;
    }

    @Override
    public List<Pubblication> findNextPublications(Date date) {
        //Hibernate.initialize
        List<Pubblication> pubblications
                = pubblicationRepository.findDistinctPubblicationsByPubblicationDateAfterOrderByPubblicationDateAsc(date);
        if (pubblications != null) {
            for (Pubblication p : pubblications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return pubblications;
    }
    
    @Override
    public List<Pubblication> findNextPublications() {
        Date today = Date.from(Instant.now().minus(1, ChronoUnit.DAYS));
        return pubblicationRepository.findDistinctPubblicationsByPubblicationDateAfterOrderByPubblicationDateAsc(today);        
    }

    @Override
    public List<Pubblication> findNextPublicationsByComicName(String comicName) {
        Date today = Date.from(Instant.now().minus(1, ChronoUnit.DAYS));
        //Hibernate.initialize
        List<Pubblication> pubblications
                = pubblicationRepository.findDistinctPubblicationsByPubblicationDateAfterAndPubblicationName(today, comicName);
        if (pubblications != null) {
            for (Pubblication p : pubblications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return pubblications;
    }

    @Override
    public List<Pubblication> findNextPublicationsByAuthorName(String name) {
        Date today = Date.from(Instant.now().minus(1, ChronoUnit.DAYS));
        //Hibernate.initialize
        List<Pubblication> pubblications = 
                pubblicationRepository.findDistinctPubblicationsByPubblicationDateAfterAndPubblicationAuthors_Pk_Author_Name(today, name);
        if (pubblications != null) {
            for (Pubblication p : pubblications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return pubblications;
    }
    
    @Override
    public List<Pubblication> findNextPublicationsByAuthorSurname(String surname) {
        Date today = Date.from(Instant.now().minus(1, ChronoUnit.DAYS));
        //Hibernate.initialize
        List<Pubblication> pubblications = 
                pubblicationRepository.findDistinctPubblicationsByPubblicationDateAfterAndPubblicationAuthors_Pk_Author_Surname(today, surname);
        if (pubblications != null) {
            for (Pubblication p : pubblications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return pubblications;
    }

    @Override
    public List<Pubblication> findNextPublicationsByAuthorNameSurname(String nameSurname) {
        Date today = Date.from(Instant.now().minus(1, ChronoUnit.DAYS));
        //Hibernate.initialize
        List<Pubblication> pubblications = 
                pubblicationRepository.findDistinctPubblicationsByPubblicationDateAfterAndPubblicationAuthors_Pk_Author_NameSurname(today, nameSurname);
        if (pubblications != null) {
            for (Pubblication p : pubblications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return pubblications;
    }

    @Override
    public List<Pubblication> findNextPublicationsByComicNameAndAuthorSurname(String comicName, String surname) {
        Date today = Date.from(Instant.now().minus(1, ChronoUnit.DAYS));
        //Hibernate.initialize
        List<Pubblication> pubblications = 
                pubblicationRepository.findDistinctPubblicationsByPubblicationDateAfterAndPubblicationNameAndPubblicationAuthors_Pk_Author_Surname(today, comicName, surname);
        if (pubblications != null) {
            for (Pubblication p : pubblications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return pubblications;
    }

    @Override
    public List<Pubblication> findNextPublicationsByComicNameAndAuthorNameSurname(String comicName, String nameSurname) {
        Date today = Date.from(Instant.now().minus(1, ChronoUnit.DAYS));
        //Hibernate.initialize
        List<Pubblication> pubblications = 
                pubblicationRepository.findDistinctPubblicationsByPubblicationDateAfterAndPubblicationNameAndPubblicationAuthors_Pk_Author_NameSurname(today, comicName, nameSurname);        
        if (pubblications != null) {
            for (Pubblication p : pubblications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return pubblications;
    }

    @Override
    public List<Pubblication> findPublicationsCurrentInNewsstandByTimeInterval(Date startDate, Date endDate) {
        return pubblicationRepository.findDistinctPubblicationsByPubblicationDateBetweenOrderByPubblicationDateAsc(startDate, endDate);
    }

    @Override
    public List<Pubblication> findPublicationsByYearAndComicName(int year, String pubName) {
        Date[] dates = getDatesFromYear(year);       
        return pubblicationRepository.findDistinctPubblicationsByPubblicationDateBetweenAndPubblicationName(dates[0], dates[1], pubName);        
    }

    @Override
    public List<Pubblication> findPublicationsByYearAndAuthorSurname(int year, String surname) {
        Date[] dates = getDatesFromYear(year);  
        //Hibernate.initialize
        List<Pubblication> publications = 
                pubblicationRepository.findDistinctPubblicationsByPubblicationDateBetweenAndPubblicationAuthors_Pk_Author_Surname(dates[0], dates[1], surname);
        if (publications != null) {
            for (Pubblication p : publications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return publications;
    }

    @Override
    public List<Pubblication> findPublicationsByYearAndAuthorNameSurname(int year, String nameSurname) {
        Date[] dates = getDatesFromYear(year);  
        //Hibernate.initialize
        List<Pubblication> publications = 
                pubblicationRepository.findDistinctPubblicationsByPubblicationDateBetweenAndPubblicationAuthors_Pk_Author_NameSurname(dates[0], dates[1], nameSurname);
        if (publications != null) {
            for (Pubblication p : publications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return publications;
    }
    
    @Override
    @Transactional(readOnly = true)
    public Pubblication findPublicationByComicNameAndNumber(String name, int number) {
        //Hibernate.initialize
        Pubblication pubblication = pubblicationRepository.findPubblicationByPubblicationNameAndNumber(name, number);        
        if (pubblication != null) {
            for (PubblicationAuthor pa : pubblication.getPubblicationAuthors()) {
                Hibernate.initialize(pa.getAuthor());
            }
        } else {
            pubblication = new Pubblication();
            pubblication.setNumber(-1);
        }
        return pubblication;
    }

    @Override
    public Pubblication findPubblicationByPubblicationNameAndPubblicationTitle(String name, String title) {
        //Hibernate.initialize
        Pubblication pubblication = pubblicationRepository.findPubblicationByPubblicationNameAndPubblicationTitle(name, title);        
        if (pubblication != null) {
            for (PubblicationAuthor pa : pubblication.getPubblicationAuthors()) {
                Hibernate.initialize(pa.getAuthor());
            }
        } else {
            pubblication = new Pubblication();
            pubblication.setNumber(-1);
        }
        return pubblication;
    }
    
    /* START AUTHOR  */
    @Override
    @Transactional(readOnly = true)
    public List<Pubblication> findPublicationsByAuthorName(String name) {
        //Hibernate.initialize
        List<Pubblication> pubblications = 
                pubblicationRepository.findDistinctPubblicationsByPubblicationAuthors_Pk_Author_Name(name);        
        if (pubblications != null) {
            for (Pubblication p : pubblications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return pubblications;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Pubblication> findPublicationsByAuthorSurname(String surname) {
        //Hibernate.initialize
        List<Pubblication> pubblications
                = pubblicationRepository.findDistinctPubblicationsByPubblicationAuthors_Pk_Author_Surname(surname);
        if (pubblications != null) {
            for (Pubblication p : pubblications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return pubblications;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Pubblication> findPublicationsByAuthorNameSurname(String nameSurname) {
        //Hibernate.initialize
        List<Pubblication> pubblications
                = pubblicationRepository.findDistinctPubblicationsByPubblicationAuthors_Pk_Author_NameSurname(nameSurname);
        if (pubblications != null) {
            for (Pubblication p : pubblications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return pubblications;
    }
    
    @Override
    public List<Pubblication> findPublicationsByComicNameAndAuthorSurname(String pubName, String surname) {
        //Hibernate.initialize
        List<Pubblication> pubblications =
                pubblicationRepository.findDistinctPubblicationsByPubblicationNameAndPubblicationAuthors_Pk_Author_Surname(pubName, surname);
        if (pubblications != null) {
            for (Pubblication p : pubblications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return pubblications;
    }

    @Override
    public List<Pubblication> findPublicationsByComicNameAndAuthorNameSurname(String pubName, String namesurname) {
        //Hibernate.initialize
        List<Pubblication> pubblications =
                pubblicationRepository.findDistinctPubblicationsByPubblicationNameAndPubblicationAuthors_Pk_Author_NameSurname(pubName, namesurname);
        if (pubblications != null) {
            for (Pubblication p : pubblications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return pubblications;
    }

    @Override
    public Pubblication findLastPubblicationByComicNameAndAuthorSurname(String pubName, String surname) {
        //Hibernate.initialize
        Pubblication pubblication =
                pubblicationRepository.findLastPubblicationByPubblicationNameAndPubblicationAuthors_Pk_Author_Surname(pubName, surname);
        if (pubblication != null) {
            for (PubblicationAuthor pa : pubblication.getPubblicationAuthors()) {
                Hibernate.initialize(pa.getAuthor());
            }
        } else {
            pubblication = new Pubblication();
            pubblication.setNumber(-1);
        }
        return pubblication;
    }
    
    @Override
    public Pubblication findPubblicationByOrdinalAndComicNameAndAuthorSurname(int ordinal, String pubName, String surname) {
        //Hibernate.initialize
        Pubblication pubblication =
                pubblicationRepository.findPubblicationByOrdinalAndPubblicationNameAndPubblicationAuthors_Pk_Author_Surname(--ordinal, pubName, surname);
        if (pubblication != null) {
            for (PubblicationAuthor pa : pubblication.getPubblicationAuthors()) {
                Hibernate.initialize(pa.getAuthor());
            }
        } else {
            pubblication = new Pubblication();
            pubblication.setNumber(-1);
        }
        return pubblication;
    }
    
    @Override
    public Pubblication findLastPubblicationByComicNameAndAuthorNameSurname(String pubName, String namesurname) {
        //Hibernate.initialize
        Pubblication pubblication =
                pubblicationRepository.findLastPubblicationByPubblicationNameAndPubblicationAuthors_Pk_Author_NameSurname(pubName, namesurname);
        if (pubblication != null) {
            for (PubblicationAuthor pa : pubblication.getPubblicationAuthors()) {
                Hibernate.initialize(pa.getAuthor());
            }
        } else {
            pubblication = new Pubblication();
            pubblication.setNumber(-1);
        }
        return pubblication;
    }
    
    @Override
    public Pubblication findPubblicationByOrdinalAndComicNameAndAuthorNameSurname(int ordinal, String pubName, String namesurname) {
        //Hibernate.initialize
        Pubblication pubblication =
                pubblicationRepository.findPubblicationByOrdinalAndPubblicationNameAndPubblicationAuthors_Pk_Author_NameSurname(--ordinal, pubName, namesurname);
        if (pubblication != null) {
            for (PubblicationAuthor pa : pubblication.getPubblicationAuthors()) {
                Hibernate.initialize(pa.getAuthor());
            }
        } else {
            pubblication = new Pubblication();
            pubblication.setNumber(-1);
        }
        return pubblication;
    }
    
    @Override
    public Pubblication findLastPubblicationByAuthorSurname(String surname) {
        //Hibernate.initialize
        Pubblication pubblication =
                pubblicationRepository.findLastPubblicationByPubblicationAuthors_Pk_Author_Surname(surname);
        if (pubblication != null) {
            for (PubblicationAuthor pa : pubblication.getPubblicationAuthors()) {
                Hibernate.initialize(pa.getAuthor());
            }
        } else {
            pubblication = new Pubblication();
            pubblication.setNumber(-1);
        }
        return pubblication;
    }
    
    @Override
    public Pubblication findPubblicationByOrdinalAndAuthorSurname(int ordinal, String surname) {
        //Hibernate.initialize
        Pubblication pubblication =
                pubblicationRepository.findPubblicationByOrdinalAndPubblicationAuthors_Pk_Author_Surname(--ordinal, surname);
        if (pubblication != null) {
            for (PubblicationAuthor pa : pubblication.getPubblicationAuthors()) {
                Hibernate.initialize(pa.getAuthor());
            }
        } else {
            pubblication = new Pubblication();
            pubblication.setNumber(-1);
        }
        return pubblication;
    }
    
    @Override
    public Pubblication findLastPubblicationByAuthorNameSurname(String namesurname) {
        //Hibernate.initialize
        Pubblication pubblication =
                pubblicationRepository.findLastPubblicationByPubblicationAuthors_Pk_Author_NameSurname(namesurname);
        if (pubblication != null) {
            for (PubblicationAuthor pa : pubblication.getPubblicationAuthors()) {
                Hibernate.initialize(pa.getAuthor());
            }
        } else {
            pubblication = new Pubblication();
            pubblication.setNumber(-1);
        }
        return pubblication;
    }
    
    @Override
    public Pubblication findPubblicationByOrdinalAndAuthorNameSurname(int ordinal, String namesurname) {
        //Hibernate.initialize
        Pubblication pubblication =
                pubblicationRepository.findPubblicationByOrdinalAndPubblicationAuthors_Pk_Author_NameSurname(--ordinal, namesurname);
        if (pubblication != null) {
            for (PubblicationAuthor pa : pubblication.getPubblicationAuthors()) {
                Hibernate.initialize(pa.getAuthor());
            }
        } else {
            pubblication = new Pubblication();
            pubblication.setNumber(-1);
        }
        return pubblication;
    }
    
    @Override
    public List<Pubblication> findPublicationsByComicNameAndYearAndAuthorSurname(String pubName, int year, String surname) {
        Date[] dates = getDatesFromYear(year);        
        //Hibernate.initialize
        List<Pubblication> pubblications =  
                pubblicationRepository.findDistinctPubblicationsByPubblicationNameAndPubblicationDateBetweenAndPubblicationAuthors_Pk_Author_Surname(pubName, dates[0], dates[1], surname);
        if (pubblications != null) {
            for (Pubblication p : pubblications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return pubblications;
    }

    @Override
    public List<Pubblication> findPublicationsByComicNameAndYearAndAuthorNameSurname(String pubName, int year, String nameSurname) {
        Date[] dates = getDatesFromYear(year);        
        //Hibernate.initialize
        List<Pubblication> pubblications =  
                pubblicationRepository.findDistinctPubblicationsByPubblicationNameAndPubblicationDateBetweenAndPubblicationAuthors_Pk_Author_NameSurname(pubName, dates[0], dates[1], nameSurname);
        if (pubblications != null) {
            for (Pubblication p : pubblications) {
                for (PubblicationAuthor pa : p.getPubblicationAuthors()) {
                    Hibernate.initialize(pa.getAuthor());
                }
            }
        }
        return pubblications;
    }
        
    /* END AUTHOR  */
    
    private Date[] getDatesFromYear(int year) {
        Date[] toRet = new Date[2];
        Calendar calendarStart = Calendar.getInstance();
        calendarStart.set(year, 0, 01, 00, 00, 01);
        toRet[0] = calendarStart.getTime();
        Calendar calendarEnd = Calendar.getInstance();
        calendarEnd.set(year, 11, 31, 23, 59, 59);
        toRet[1] = calendarEnd.getTime();
        
        return toRet;
    }
           
}
