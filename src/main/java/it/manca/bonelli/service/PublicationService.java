package it.manca.bonelli.service;

import java.util.List;

import it.manca.bonelli.model.Pubblication;
import java.util.Date;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface PublicationService {

    void save(Pubblication pub);
    void delete(Pubblication pub);
    void update(Pubblication pub);
        
    /* START COUNT */
    int countPublicationsByAuthorSurname(String surname);
    int countPublicationsByYearAndAuthorSurname(int year, String surname);
    
    int countPublicationsByAuthorNameSurname(String nameSurname);
    int countPublicationsByYearAndAuthorNameSurname(int year, String nameSurname);
    
    int countPublicationsByComicNameAndAuthorSurname(String comic_name, String surname);
    int countPublicationsByComicNameAndYearAndAuthorSurname(String comic_name, 
            int year, String surname);
    
    int countPublicationsByNameAndAuthorNameSurname(String comic_name, String nameSurname);
    int countPublicationsByNameAndYearAndAuthorNameSurname(String comic_name, 
            int year, String nameSurname);
    /* END COUNT */
    
    /* START IN NEWSSTAND */
    List<Pubblication> findPublicationsCurrentInNewsstand();
    List<Pubblication> findPublicationsCurrentInNewsstandByPubblicationName(String comicName);
    List<Pubblication> findPublicationsCurrentInNewsstandByAuthorName(String name);
    List<Pubblication> findPublicationsCurrentInNewsstandByAuthorSurname(String surname);
    List<Pubblication> findPublicationsCurrentInNewsstandByAuthorNameSurname(String nameSurname);
    List<Pubblication> findPublicationsCurrentInNewsstandByComicNameAndAuthorSurname(String comic_name, String surname);
    List<Pubblication> findPublicationsCurrentInNewsstandByComicNameAndAuthorNameSurname(String comic_name, String nameSurname);
    List<Pubblication> findPublicationsCurrentInNewsstandByTimeInterval(Date startDate, Date endDate);
    /* END IN NEWSSTAND */

    /* START NEXT */
    List<Pubblication> findNextPublications(Date date);
    List<Pubblication> findNextPublications();
    List<Pubblication> findNextPublicationsByComicName(String comicName);
    List<Pubblication> findNextPublicationsByAuthorName(String name);
    List<Pubblication> findNextPublicationsByAuthorSurname(String surname);
    List<Pubblication> findNextPublicationsByAuthorNameSurname(String nameSurname);
    List<Pubblication> findNextPublicationsByComicNameAndAuthorSurname(String comicName, String surname);
    List<Pubblication> findNextPublicationsByComicNameAndAuthorNameSurname(String comicName, String nameSurname);      
    /* END NEXT */
    
    /* START SEARCH BY YEAR */
    List<Pubblication> findPublicationsByYearAndComicName(int year, String pubName);
    List<Pubblication> findPublicationsByYearAndAuthorSurname(int year, String surname);
    List<Pubblication> findPublicationsByYearAndAuthorNameSurname(int year, String nameSurname);    
    /* END SEARCH BY YEAR */
    
    /* START COMIC NAME AND NUMBER  */
    Pubblication findPublicationByComicNameAndNumber(String name, int number);
    /* END COMIC NAME AND NUMBER  */
    
    Pubblication findPubblicationByPubblicationNameAndPubblicationTitle(String name, String title);
    
    /* START AUTHOR  */
    List<Pubblication> findPublicationsByAuthorName(String name);
    List<Pubblication> findPublicationsByAuthorSurname(String surname);
    List<Pubblication> findPublicationsByAuthorNameSurname(String nameSurname);
    List<Pubblication> findPublicationsByComicNameAndAuthorSurname(String pubName, String surname);
    List<Pubblication> findPublicationsByComicNameAndAuthorNameSurname(String pubName,String namesurname);
    
    Pubblication findLastPubblicationByComicNameAndAuthorSurname(String pubName, String surname);
    Pubblication findPubblicationByOrdinalAndComicNameAndAuthorSurname(int ordinal, String pubName, String surname);
    
    Pubblication findLastPubblicationByComicNameAndAuthorNameSurname(String pubName, String namesurname);
    Pubblication findPubblicationByOrdinalAndComicNameAndAuthorNameSurname(int ordinal, String pubName, String namesurname);
    
    Pubblication findLastPubblicationByAuthorSurname(String surname);
    Pubblication findPubblicationByOrdinalAndAuthorSurname(int ordinal, String surname);
    
    Pubblication findLastPubblicationByAuthorNameSurname(String namesurname);
    Pubblication findPubblicationByOrdinalAndAuthorNameSurname(int ordinal, String namesurname);
    
    List<Pubblication> findPublicationsByComicNameAndYearAndAuthorSurname(String pubName, int year, String surname);
    List<Pubblication> findPublicationsByComicNameAndYearAndAuthorNameSurname(String pubName, int year, String nameSurname);       
    /* END AUTHOR  */
    
    List<Pubblication> findAllPublications();
    List<Pubblication> findPublicationsByComicName(String name);
    Pubblication findLastPubblicationByComicName(String name);
    Pubblication findPubblicationByOrdinalAndComicName(int ordinal, String name);
    List<Pubblication> findPublicationsByComicNameAndYear(String name, int year);
    
}
