package it.manca.bonelli.service;

import it.manca.bonelli.model.Author;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Marco Manca
 */
@Transactional
public interface AuthorService {
    void save(Author author);
    void delete(Author author);
    void update(Author author);
    
    List<Author> findAllAuthors();
    List<Author> findAuthorsBySurname(String surname);
    Author findAuthorByNameSurname(String nameSurname);
    Author findAuthorByNameAndSurname(String name, String surname);
    
}
