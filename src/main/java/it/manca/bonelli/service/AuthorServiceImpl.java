package it.manca.bonelli.service;

import it.manca.bonelli.model.Author;
import it.manca.bonelli.repository.AuthorRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Marco Manca
 */
@Service("authorService")
@Transactional      
public class AuthorServiceImpl implements AuthorService{
    @Autowired
    AuthorRepository authorRepository;

    @Override
    public void save(Author author) {
        authorRepository.save(author);
    }

    @Override
    public void delete(Author author) {
        authorRepository.delete(author);
    }

    @Override
    public void update(Author author) {
        authorRepository.save(author);
    }

    @Override
    public List<Author> findAllAuthors() {
        return authorRepository.findAll();
    }

    @Override
    public List<Author> findAuthorsBySurname(String surname) {
        return authorRepository.findAuthorsBySurname(surname);
    }

    @Override
    public Author findAuthorByNameSurname(String nameSurname) {
        List<Author> authors = authorRepository.findAuthorByNameSurname(nameSurname);
        if(authors != null && !authors.isEmpty()) {
            return authors.get(0);
        } else {
            return null;
        }
    }

    @Override
    public Author findAuthorByNameAndSurname(String name, String surname) {
        List<Author> authors = authorRepository.findAuthorByNameAndSurname(name, surname);
        if(authors != null && !authors.isEmpty()) {
            return authors.get(0);
        } else {
            return null;
        }
    }
    
}
