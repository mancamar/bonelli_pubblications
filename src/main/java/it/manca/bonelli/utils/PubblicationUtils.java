package it.manca.bonelli.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.manca.bonelli.model.Pubblication;
import it.manca.bonelli.model.PubblicationAuthor;
import it.manca.bonelli.model.Tag;
import it.manca.bonelli.service.AuthorService;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.jsoup.select.Selector;
import it.manca.bonelli.service.PublicationService;

/**
 *
 * @author marcomanca
 */
public class PubblicationUtils {

    public static List<Pubblication> getPubblicationList(PublicationService pubService, AuthorService authorService, Document doc, String type, String shopBaseUrl) {
        List<Pubblication> pubblicationList = new LinkedList<>();
        String smallCoverURL = "", schedaURL = "";
        
        //old div
        //Elements schede = doc.select("div.scheda");
        Elements schede = doc.select("div.anteprima_ricerca_archivio");
        for (Element scheda : schede) {            
            //Element attributesScheda = scheda.select("article > div.cont_foto > a").first();
            Element attributesScheda = scheda.select("div.cont_foto > a").first();
            schedaURL = attributesScheda.attr("href");
            String url = ""; 
            if(!schedaURL.startsWith("http://shop.sergiobonelli.it") && !schedaURL.startsWith("https://shop.sergiobonelli.it")) {
                if(schedaURL.startsWith("/")) {
                    url = shopBaseUrl.concat(schedaURL);
                } else {
                    url = shopBaseUrl.concat("/").concat(schedaURL);
                }
            } else {
                url = schedaURL;
            }
            
            Pubblication newPubblication = getPubblication(pubService, authorService, url, type);
            if(newPubblication == null) {
                continue;
            }
            //Element attributesFoto = scheda.select("article > div.cont_foto > a > img").first();
            Element attributesFoto = scheda.select("div.cont_foto > a > img").first();
            smallCoverURL = attributesFoto.attr("src");

            newPubblication.setSmallCoverURL(smallCoverURL);
            if(newPubblication.getBigCoverURL() == null || newPubblication.getBigCoverURL().equals("") ||
                    newPubblication.getBigCoverURL().equals("https://shop.sergiobonelli.it/")) {
                    newPubblication.setBigCoverURL(smallCoverURL.replace("-1/325", "1000/-1"));
            }
            pubblicationList.add(newPubblication);
        }
        return pubblicationList;
    }

    public static Pubblication getPubblication(PublicationService pubService, AuthorService authorService, String pubblicationSchedaURL, String type) {
        try {
            System.out.println("Pubblication url ".concat(pubblicationSchedaURL));
            Document doc = Jsoup.connect(pubblicationSchedaURL).timeout(0).get();
            String bigCoverURL = "", smallCoverUrl = "", summary = "";
            String pubblicationName = "", pubblicationPeriodicity = "";
            String pubblicationTitle = "";
            int pubblicationNumber;
            Date pubblicationDate = null;

            Elements publicationNameEl = doc.select("p.tag_92 > span.valore");
            pubblicationName = publicationNameEl.text();
            if(pubblicationName == null) {
                return null;
            }
            String prevPubblicationUrl = "";
            if (type.equals("oldPubblication")) {
                try {
                    Elements prevPubblicationUrlEl = doc.select("div.nav_albo_cont_element > p.prev > a");
                    prevPubblicationUrl = prevPubblicationUrlEl.attr("href");
                } catch (Selector.SelectorParseException ex) {

                }
            }

            Elements publicationNumberEl = doc.select("p.tag_1 > span.valore");
            if(publicationNumberEl == null || publicationNumberEl.text().equals("")) {
                return null;
            }
            pubblicationNumber = Integer.valueOf(publicationNumberEl.text());
            
            Pubblication pubblicationFromDb = pubService.findPublicationByComicNameAndNumber(pubblicationName, pubblicationNumber);
            if(pubblicationFromDb != null && pubblicationFromDb.getNumber() > -1) {
                pubblicationFromDb.setPreviousPubblicationURL(prevPubblicationUrl);
                return pubblicationFromDb;
            }
            
            Elements pubblicationPeriodicityEL = doc.select("p.tag_3 > span.valore");
            pubblicationPeriodicity = pubblicationPeriodicityEL.text();

            Elements pubblicationTitleEL = doc.select("h1.titolo_articolo");
            pubblicationTitle = pubblicationTitleEL.text();

            Elements pubblicationDateEl = doc.select("p.tag_2 > span.valore");
            String[] dateSplit = pubblicationDateEl.text().split("/");
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, Integer.parseInt(dateSplit[2]));
            cal.set(Calendar.MONTH, Integer.parseInt(dateSplit[1]) - 1);
            cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateSplit[0]));
            pubblicationDate = cal.getTime();

            Elements bigCoverURLEl;
            if(type.equals("oldPubblication")) {
                bigCoverURLEl = doc.select("div.foto_articolo > img");            
            } else {
                bigCoverURLEl = doc.select("img.gc-display-display");
            }
            if (bigCoverURLEl != null && !bigCoverURLEl.isEmpty()) {
                bigCoverURL = bigCoverURLEl.attr("src");
                smallCoverUrl = bigCoverURL.replace("-1/-1", "-1/229");
            } else {
                bigCoverURLEl = doc.select("div.foto_articolo > img");            
                if (bigCoverURLEl != null) {
                    bigCoverURL = bigCoverURLEl.attr("src");
                    smallCoverUrl = bigCoverURL.replace("-1/-1", "-1/229");
                }
            }
            if(!bigCoverURL.startsWith("http://shop.sergiobonelli.it") 
                    && !bigCoverURL.startsWith("https://shop.sergiobonelli.it")) {
                bigCoverURL = "https://shop.sergiobonelli.it/".concat(bigCoverURL);
            }
            if(!smallCoverUrl.startsWith("http://shop.sergiobonelli.it") 
                    && !smallCoverUrl.startsWith("https://shop.sergiobonelli.it")) {
                smallCoverUrl = "https://shop.sergiobonelli.it/".concat(smallCoverUrl);
            }
//            Elements summaryEl = doc.select("div.testo_articolo > p");
//            if(summaryEl != null) {
//                summary = summaryEl.text();                
//            }
            Elements summaryEl = doc.select("script[type=application/ld+json]");
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNodeRoot = mapper.readTree(summaryEl.html());
            JsonNode jsonNodeSummary = jsonNodeRoot.get("text");
            if (jsonNodeSummary != null) {
                summary = jsonNodeSummary.asText();
            }
            //System.out.println("Summary ".concat(summary));
            List<PubblicationAuthor> pubblicationAuthors = AuthorsUtils.getAuthors(doc, authorService);
            
            List<Tag> tags = TagsUtils.getTags(doc);
            Pubblication p = new Pubblication(pubblicationDate, pubblicationNumber, 
                    pubblicationPeriodicity, pubblicationName, pubblicationTitle, 
                    smallCoverUrl, bigCoverURL, pubblicationSchedaURL,  
                    summary, prevPubblicationUrl, tags);
            for(PubblicationAuthor pa :  pubblicationAuthors) {
                pa.setPubblication(p);
            }
            p.getPubblicationAuthors().addAll(pubblicationAuthors);
            
            return p;
        } catch (IOException ex) {
            Logger.getLogger(PubblicationUtils.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    private static List<String> initAllowedPubblications() {
        List<String> allowedPubblications = new LinkedList<>();
        allowedPubblications.add("lukas");
        allowedPubblications.add("julia");
        allowedPubblications.add("dylandog");
        allowedPubblications.add("romanzieminiserie");
        allowedPubblications.add("morganlost");
        allowedPubblications.add("lestorie");
        allowedPubblications.add("adamwild");
        allowedPubblications.add("orfani");
        return allowedPubblications;
    }       
}
