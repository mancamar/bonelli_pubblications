package it.manca.bonelli.utils;

import it.manca.bonelli.model.Tag;
import java.util.LinkedList;
import java.util.List;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author marcomanca
 */
public class TagsUtils {

    public static List<Tag> getTags(Document doc) {
        List<Tag> tags = new LinkedList<>();
        Elements tagElements = doc.select("meta[name=Keywords]");
        if (tagElements != null) {
            String tagsString = tagElements.attr("content");
            if (tagsString.contains(",")) {
                String[] tagSplit = tagsString.split(",");
                for (String tag : tagSplit) {
                    //tag: dylan dog
                    //tag link: http://shop.sergiobonelli.it/ricerca?q=dylan dog&idcanale=3
                    String tagLink = "http://shop.sergiobonelli.it/ricerca?q=".concat(tag)
                            .concat("&idCanale=3");
                    tags.add(new Tag(tag, tagLink));
                }
            }
        }
        return tags;
    }
}
