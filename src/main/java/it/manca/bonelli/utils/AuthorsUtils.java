package it.manca.bonelli.utils;

import it.manca.bonelli.model.Author;
import it.manca.bonelli.model.AuthorRole;
import it.manca.bonelli.model.PubblicationAuthor;
import it.manca.bonelli.service.AuthorService;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author marcomanca
 */
public class AuthorsUtils {

    public static List<PubblicationAuthor> getAuthors(Document doc, AuthorService authorService) {
        List<PubblicationAuthor> toRet = new ArrayList<>(6);        
        String subject = "", script = "", illustration = "", color = "", cover = "";
        
        Elements coverEl = doc.select("p.tag_7 > span.valore");
        if (coverEl.size() > 0) {
            cover = coverEl.text().trim();
            toRet.addAll(getAuthors(authorService, cover, AuthorRole.cover));
        }
        
        Elements subjctEl = doc.select("p.tag_4 > span.valore");
        if (subjctEl.size() > 0) {
            subject = subjctEl.text().trim();
            toRet.addAll(getAuthors(authorService, subject, AuthorRole.subject));
        } else {
            Elements summaryAuthorsP = doc.select("div.testo_articolo");
            if (summaryAuthorsP.isEmpty()) {
                //unstructured data: all the authors contained inside a p element. 
                //I retrieve the authros name from the meta tag with attribute name=section
                Elements metaSection = doc.select("meta[name=sections]");
                String authorsText = metaSection.attr("content").trim();
                String[] authorsSplit = authorsText.split(",");
                for (String auth : authorsSplit) {
                    if (!containsComicName(auth)) {
                        toRet.addAll(getAuthors(authorService, auth, AuthorRole.unknown));
                    }
                }
            } else {
                //unstructured data: one p element per story
                //inside each p there are multiple strong elements containing the authros name
                //depending on the strong element number I'm able to undestand the author role 
                //(see getUnstructuredAuthors)
                List<PubblicationAuthor> unstructuredAuthors = getUnstructuredAuthors(authorService, summaryAuthorsP);
                if(!unstructuredAuthors.isEmpty()) {
                    toRet.addAll(unstructuredAuthors);
                } else {
                    Elements metaSection = doc.select("meta[name=sections]");
                    String authorsText = metaSection.attr("content").trim();
                    String[] authorsSplit = authorsText.split(",");
                    for (String auth : authorsSplit) {
                        if (!containsComicName(auth)) {
                            toRet.addAll(getAuthors(authorService, auth, AuthorRole.unknown));
                        }
                    }
                }
            }
            return toRet;
        }

        Elements scriptEl = doc.select("p.tag_5 > span.valore");
        if (scriptEl.size() > 0) {
            script = scriptEl.text();
            toRet.addAll(getAuthors(authorService, script, AuthorRole.script));
        }
        Elements illustrationEl = doc.select("p.tag_6 > span.valore");
        if (illustrationEl.size() > 0) {
            illustration = illustrationEl.text();
            toRet.addAll(getAuthors(authorService, illustration, AuthorRole.illustration));
        }
        Elements colorEl = doc.select("p.tag_20 > span.valore");
        if (colorEl.size() > 0) {
            color = colorEl.text();
            toRet.addAll(getAuthors(authorService, color, AuthorRole.color));
        }
        
        return toRet;
    }

    private static List<PubblicationAuthor> getAuthors(AuthorService authorService, String authorText, AuthorRole role) {
        authorText = authorText.trim();        
        List<PubblicationAuthor> toRet = new ArrayList<>(1);
        if (!areMultipleAuthors(authorText)) {
            //2021-06-03: nuovo formato cognome nome
            String[] authorSplit = authorText.split(" ");
            if(authorSplit.length == 2) {
                authorText = authorSplit[1].concat(" ").concat(authorSplit[0]);
            } else {
                //non so come fare perchè potrebbero essere due nomi come due cognomi
            }
            //controllo se l'autore esiste nel db
            Author auth = authorService.findAuthorByNameSurname(authorText);
            if (auth == null) {               
                auth = getAuthorInfo(authorService, authorText);
            }
            if (auth != null
                    && !"".equals(auth.getNameSurname())) {
                PubblicationAuthor pubblicationAuthor = new PubblicationAuthor(auth, role);
                auth.getPubblicationAuthors().add(pubblicationAuthor);
                toRet.add(new PubblicationAuthor(auth, role));
            }
        } else {
            //multiple authors  
            authorText = cleanMultipleAuthors(authorText);
            String[] authors = authorText.split(",");
            for (String author : authors) {
                Author auth;
                if (author.startsWith(" colori") && role.compareTo(AuthorRole.cover) == 0) {
                    //controller se esiste nel db
                    //2021-06-03: nuovo formato cognome nome
                    String[] authorSplit = author.substring(7).trim().split(" ");
                    if (authorSplit.length == 2) {
                        author = authorSplit[1].concat(" ").concat(authorSplit[0]);
                    } else {
                        //non so come fare perchè potrebbero essere due nomi come due cognomi
                    }
                    auth = authorService.findAuthorByNameSurname(author);
                    if(auth == null) {
                        auth = getAuthorInfo(authorService, author);                    
                    }
                    if (auth != null
                        && !"".equals(auth.getNameSurname())) {
                        PubblicationAuthor pubblicationAuthor = new PubblicationAuthor(auth, AuthorRole.cover_color);
                        auth.getPubblicationAuthors().add(pubblicationAuthor);
                        toRet.add(pubblicationAuthor);
                    }
                } else {
                    //controller se esiste nel db
                    //2021-06-03: nuovo formato cognome nome
                    String[] authorSplit = author.substring(7).trim().split(" ");
                    if (authorSplit.length == 2) {
                        author = authorSplit[1].concat(" ").concat(authorSplit[0]);
                    } else {
                        //non so come fare perchè potrebbero essere due nomi come due cognomi
                    }
                    auth = authorService.findAuthorByNameSurname(author);
                    if(auth == null) {
                        auth = getAuthorInfo(authorService, author);
                    }
                    if (auth != null
                        && !"".equals(auth.getNameSurname())) {
                        PubblicationAuthor pubblicationAuthor = new PubblicationAuthor(auth, role);
                        auth.getPubblicationAuthors().add(pubblicationAuthor);
                        toRet.add(pubblicationAuthor);
                    }
                }
                
            }
        }
        return toRet;
    }

    private static boolean areMultipleAuthors(String authorText) {
        return authorText.contains(",")
                || authorText.contains(" e ")
                || authorText.contains(" & ")
                || authorText.contains("/")
                || authorText.contains(" in collaborazione con ")
                || authorText.contains(" con la collaborazione di ");
    }

    private static String cleanMultipleAuthors(String authorText) {
        authorText = authorText.replaceAll(" e ", ",");
        authorText = authorText.replaceAll("Raul & ", "Raul Cestaro,");
        authorText = authorText.replaceAll(" & ", ",");
        authorText = authorText.replaceAll(" in collaborazione con ", ",");
        authorText = authorText.replaceAll(" con la collaborazione di ", ",");
        authorText = authorText.replaceAll("/", ",");
        //authorText = authorText.replaceAll(", colori ", ",");
        return authorText;
    }

    private static Author getAuthorInfo(AuthorService authorService, String authorText) {  
        authorText = authorText.trim();
        Author a = null;
        if (authorText.contains(" ")) {
            //name + surname
            String[] authorSplit = authorText.split(" ");
            if (authorSplit.length > 2) {
                //multiple names or surnames
                if(authorSplit.length == 3 && 
                      (authorText.contains("De") 
                        || authorText.contains("Di") 
                        || authorText.contains("D'")
                        || authorText.contains("Del")
                        || authorText.contains("Della")
                        || authorText.contains("Eleuteri"))) {                                                           
                        a = new Author(authorSplit[0], authorSplit[1].concat(" ")
                                .concat(authorSplit[2]), authorText);                                            
                } else if(authorSplit.length == 3 && 
                      (authorText.contains("Carlo Raffaele")
                        || authorText.contains("Angel"))) {                    
                  a = new Author(authorSplit[0].concat(" ")
                          .concat(authorSplit[1]), authorSplit[2], authorText);                  
                } else if(authorSplit.length == 3 && 
                      authorText.contains("(Magnus)")
                      || authorText.contains("(Majo)")) {
                    String nickName = authorSplit[3].replace("(", "");
                    nickName = nickName.replace(")", "");
                  a =  new Author(authorSplit[0], authorSplit[1], nickName);                  
                } else if(authorSplit.length == 4 && 
                      authorText.contains("De La")) {
                    a = new Author(authorSplit[0], authorSplit[1].concat(" ")
                          .concat(authorSplit[2]).concat(authorSplit[3]), authorText);                      
                } else {
                    a = new Author(authorText);                    
                }
            } else {
                a =  new Author(authorSplit[0], authorSplit[1], authorText);
            }
        } else {
            //only name
            a = new Author(authorText);
        }
        authorService.save(a);
        return a;
    }

    private static boolean containsComicName(String txt) {
        if (txt.contains("Tex") 
                || txt.contains("Dylan")
                || txt.contains("Julia")
                || txt.contains("Morgan")
                || txt.contains("Nathan")
                || txt.contains("Dragonero")
                || txt.contains("Storie")
                || txt.contains("Mister")
                || txt.contains("Zagor")
                || txt.contains("Nick")
                || txt.contains("Commandante")
                || txt.contains("Legs")
                || txt.contains("Magico")
                || txt.contains("Brandon")
                || txt.contains("Jonathan")
                || txt.contains("Orfani")
                || txt.contains("Napoleone")
                || txt.contains("Saguaro")
                || txt.contains("Adam")
                || txt.contains("Brad")
                || txt.contains("Lukas")
                || txt.contains("Demian")
                || txt.contains("Cassidy")
                || txt.contains("Lilith")
                || txt.contains("Gea")
                || txt.contains("Shanghai")
                || txt.contains("Gregory")
                || txt.contains("Miniserie")
                || txt.contains("Mercurio")
                || txt.contains("Jan")
                || txt.contains("Volto")
                || txt.contains("Graystorm")
                || txt.contains("Caravan")
                || txt.contains("Avventura")
                || txt.contains("4Hoods")
                || txt.contains("Creepy")
                || txt.contains("Commissario")
                || txt.contains("One Shot")
                || txt.contains("Deadwood")
                || txt.contains("Leo Pulp")
                || txt.contains("Cani Sciolti")
                || txt.contains("Vendita Albi")
                || txt.contains("Fumetti")
                ) {
            return true;
        } else {
            return false;
        }
    }

    private static List<PubblicationAuthor> getUnstructuredAuthors(AuthorService authorService, Elements summaryAuthors) {
        List<PubblicationAuthor> toRet = new ArrayList<>(5);
        Elements summaryAuthorsParagraph = summaryAuthors.select("p");
        if(summaryAuthorsParagraph.isEmpty()) {
            summaryAuthorsParagraph = summaryAuthors;
        }
        for (int i = 0; i < summaryAuthorsParagraph.size(); i++) {
            Element pEl = summaryAuthorsParagraph.get(i);
            Elements genericEl = pEl.select("strong");
            if(genericEl.isEmpty()) {
                genericEl = pEl.select("b");
            }                        
            switch (genericEl.size()) {
                case 1:
                    if(pEl.text().contains("Soggetto, sceneggiatura, disegni e copertina:")) {
                        String allAuthor = genericEl.get(0).text().trim();
                        toRet.addAll(getAuthors(authorService, allAuthor, AuthorRole.subject));
                        toRet.addAll(getAuthors(authorService, allAuthor, AuthorRole.script));
                        toRet.addAll(getAuthors(authorService, allAuthor, AuthorRole.illustration));                        
                        toRet.addAll(getAuthors(authorService, allAuthor, AuthorRole.cover));                        
                    } else if(pEl.text().contains("Soggetto, sceneggiatura e disegni")) {
                        /*                           
                           //1- subject, script, illustration
                           <p>
                           <strong>La grande baraonda</strong>
                           <br>Soggetto, sceneggiatura e disegni:
                           <strong>Gigi Simeoni</strong>
                           </p>
                         */                    
                        String allAuthor = genericEl.get(0).text().trim();
                        toRet.addAll(getAuthors(authorService, allAuthor, AuthorRole.subject));
                        toRet.addAll(getAuthors(authorService, allAuthor, AuthorRole.script));
                        toRet.addAll(getAuthors(authorService, allAuthor, AuthorRole.illustration));                        
                    }
                    break;
                case 2:
                    //1-Soggetto e sceneggiatura:
                    //2-  Disegni e copertina: 
                    if((pEl.text().contains("Soggetto e sceneggiatura:") || pEl.text().contains("Soggetto e Sceneggiatura: ")) &&
                            pEl.text().contains("Disegni e copertina:")) {
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.script));
                        toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.illustration));
                        toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.cover));
                    } else if(pEl.text().contains("Soggetto, sceneggiatura, disegni e colori")) {
                        /*
                           //1- title
                           //2- subject, script, illustration, color
                           <p>
                           <strong>La grande baraonda</strong>
                           <br>Soggetto, sceneggiatura, disegni e colori:
                           <strong>Fabio Celoni</strong>
                           </p>
                         */                    
                        String allAuthor = genericEl.get(1).text().trim();
                        toRet.addAll(getAuthors(authorService, allAuthor, AuthorRole.subject));
                        toRet.addAll(getAuthors(authorService, allAuthor, AuthorRole.script));
                        toRet.addAll(getAuthors(authorService, allAuthor, AuthorRole.illustration));
                        toRet.addAll(getAuthors(authorService, allAuthor, AuthorRole.color));
                    }
                    break;
                case 3:                          
                    if (pEl.text().contains("Soggetto, sceneggiatura e disegni:")) {
                        /*
                       //1- title
                       //2- subject, script, illustration
                       //3- color
                        * <p>
                       <strong>Di mostri, incubi e ragazze</strong>
                       <br>Soggetto, sceneggiatura e disegni:
                       <strong>Alessandro Baggi</strong>
                       <br>Colori:
                       <strong>Riccardo Riboldi</strong>
                       </p>
                     */
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.script));
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.illustration));
                        if (pEl.text().contains("Colori:")) {
                            String color = genericEl.get(1).text();
                            toRet.addAll(getAuthors(authorService, color, AuthorRole.color));
                            String cover = genericEl.get(2).text();
                            toRet.addAll(getAuthors(authorService, cover, AuthorRole.cover));
                        } else {
                            String cover = genericEl.get(1).text();
                            toRet.addAll(getAuthors(authorService, cover, AuthorRole.cover));
                        }
                    } else if (pEl.text().contains("Soggetto e sceneggiatura:") || pEl.text().contains("Soggetto e Sceneggiatura: ")) {
                        //dyd
                        //1- Soggetto e sceneggiatura
                        //2- Disegni
                        //3- Copertina
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.script));
                        toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.illustration));
                        toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.cover));
                    } else if(pEl.text().contains("Disegni e colori:")) {
                        /*
                       //1- title
                       //2- subject, script
                       //3- illustration, color
                     * <p>
                        <strong>Ancora un lungo addio</strong>
                        <br>Soggetto e sceneggiatura: 
                        <strong>Paola Barbato</strong>
                        <br>Disegni e colori: 
                        <strong>Carmine Di Giandomenico</strong>
                       </p>
                     */                                
                        toRet.addAll(getAuthors(authorService, genericEl.get(1).text().trim(), AuthorRole.subject));
                        toRet.addAll(getAuthors(authorService, genericEl.get(1).text().trim(), AuthorRole.script));                        
                        String illustrationAndColor = genericEl.get(2).text();
                        toRet.addAll(getAuthors(authorService, illustrationAndColor, AuthorRole.illustration)); 
                        toRet.addAll(getAuthors(authorService, illustrationAndColor, AuthorRole.color));
                    } else if (pEl.text().contains("Soggetto:") &&
                            pEl.text().contains("Sceneggiatura:") &&
                            pEl.text().contains("Disegni e copertina:")) { 
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                        toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.script));
                        toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.illustration));
                        toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.cover));
                    } else {
                        if (pEl.text().contains("Disegni:")) {                            
                            toRet.addAll(getAuthors(authorService, genericEl.get(0).text().trim(), AuthorRole.subject));
                            toRet.addAll(getAuthors(authorService, genericEl.get(0).text().trim(), AuthorRole.script));
                            String illustration = genericEl.get(1).text();
                            toRet.addAll(getAuthors(authorService, illustration, AuthorRole.illustration));
                            if(genericEl.get(2) != null) {
                                toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.cover));
                            }
                        }
                    }
                    
                    
                    
                    
                    break;
                case 4:                    
                    //1- title
                    //2- subject and script
                    //3- illustration
                    //4- colors
                    /*
                        * <p>
                       <strong>Golden Queen</strong>
                       <br>Soggetto e sceneggiatura:
                       <strong>Luca Barbieri</strong>
                       <br>Disegni:
                       <strong>Andrea Venturi</strong>
                       <br>Colori:
                       <strong>Oscar Celestini</strong>
                       </p>
                     */
                    if ((pEl.text().contains("Soggetto e sceneggiatura") || pEl.text().contains("Soggetto e Sceneggiatura: ")) &&
                            pEl.text().contains("Montanari")) {
                        //dyd
                        //1- Soggetto e sceneggiatura
                        //2- Disegni
                        //3- Copertina
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.script));
                        toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.illustration));
                        toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.illustration));
                        toRet.addAll(getAuthors(authorService, genericEl.get(3).text(), AuthorRole.cover));
                    } else if ((pEl.text().contains("Soggetto e sceneggiatura") || pEl.text().contains("Soggetto e Sceneggiatura: ")) &&
                            pEl.text().contains("Cesare Valeri")) {
                        //dyd
                        //1- Soggetto e sceneggiatura
                        //2- Disegni
                        //3- Disegni
                        //4- Copertina
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.script));
                        toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.illustration));
                        toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.illustration));
                        toRet.addAll(getAuthors(authorService, genericEl.get(3).text(), AuthorRole.cover));
                    } else if (pEl.text().contains("Soggetto:") &&
                            (pEl.text().contains("Sceneggiatura:") || pEl.text().contains("sceneggiatura:"))) {                        
                        //dyd
                        //1- Soggetto
                        //2- Sceneggiatura
                        //3- Disegni
                        //4- Copertina
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                        toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.script));
                        toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.illustration));
                        toRet.addAll(getAuthors(authorService, genericEl.get(3).text(), AuthorRole.cover));                        
                    } else if ((pEl.text().contains("Soggetto e sceneggiatura: ") || pEl.text().contains("Soggetto e Sceneggiatura: "))&& 
                            pEl.text().contains("Colori: ")) {
                        //dyd
                        //1- Soggetto - Sceneggiatura
                        //2- Disegni
                        //3- Copertina
                        //4- Colori
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.script));
                        toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.illustration));
                        toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.cover));                        
                        toRet.addAll(getAuthors(authorService, genericEl.get(3).text(), AuthorRole.color));                        
                    } else if ((pEl.text().contains("Soggetto e sceneggiatura") || pEl.text().contains("Soggetto e Sceneggiatura: ")) &&
                            pEl.text().contains("Con la collaborazione di")) {
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.script));
                        toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.script));
                        toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.illustration));
                        toRet.addAll(getAuthors(authorService, genericEl.get(3).text(), AuthorRole.cover));  
                    } if ((pEl.text().contains("Soggetto e sceneggiatura") || pEl.text().contains("Soggetto e Sceneggiatura: ")) &&
                            pEl.text().contains("-")) {
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.script));
                        toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.illustration));
                        if(genericEl.get(3).text().contains("In questo numero:")) {
                            toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.cover)); 
                        } else {
                            toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.illustration));
                            toRet.addAll(getAuthors(authorService, genericEl.get(3).text(), AuthorRole.cover)); 
                        }
                         
                    }else {  
                        if(genericEl.get(3).text().contains("In questo numero")) {
                            if(pEl.text().contains("/")) {
                                if(pEl.text().indexOf("/") > pEl.text().indexOf("Disegni")) {
                                    toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                                    toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.script));
                                    String illustration = genericEl.get(1).text();
                                    toRet.addAll(getAuthors(authorService, illustration, AuthorRole.illustration));
                                    String cover = genericEl.get(2).text();
                                    toRet.addAll(getAuthors(authorService, cover, AuthorRole.cover));
                                } else {
                                    toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                                    toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.script));                                    
                                    String illustration = genericEl.get(1).text();
                                    toRet.addAll(getAuthors(authorService, illustration, AuthorRole.illustration));
                                    toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.cover));                                    
                                }
                            } else {
                                toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                                toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.script));
                                String illustration = genericEl.get(1).text();
                                toRet.addAll(getAuthors(authorService, illustration, AuthorRole.illustration));
                                String cover = genericEl.get(2).text();
                                toRet.addAll(getAuthors(authorService, cover, AuthorRole.cover));
                            }
                        } else {
                            if(pEl.text().contains("Soggetto:") && pEl.text().contains("Sceneggiatura:")) {
                                toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                                toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.script));
                                String illustration = genericEl.get(2).text();
                                toRet.addAll(getAuthors(authorService, illustration, AuthorRole.illustration));
                                String color = genericEl.get(3).text();
                                toRet.addAll(getAuthors(authorService, color, AuthorRole.cover));
                            } else if(pEl.text().contains("Soggetto e sceneggiatura: ") && 
                                pEl.text().contains("Disegni: ") &&
                                pEl.text().contains("Copertina: ") &&
                                pEl.text().contains(" e ")) {
                                if(pEl.text().indexOf(" e ", pEl.text().indexOf("tura: ")) > pEl.text().indexOf("Disegni")) {
                                    toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                                    toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.script));
                                    toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.illustration));                            
                                    toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.illustration));                            
                                    toRet.addAll(getAuthors(authorService, genericEl.get(3).text(), AuthorRole.cover)); 
                                } else {
                                    toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                                    toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.script));
                                    toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.subject));
                                    toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.script));
                                    toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.illustration));                                    
                                    toRet.addAll(getAuthors(authorService, genericEl.get(3).text(), AuthorRole.cover)); 
                                } 
                            } else {
                                toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                                toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.script));
                                String illustration = genericEl.get(1).text();
                                toRet.addAll(getAuthors(authorService, illustration, AuthorRole.illustration));
                                String color = genericEl.get(3).text();
                                toRet.addAll(getAuthors(authorService, color, AuthorRole.cover));
                            }
                        }
                    }
                    break;
                case 5: 
                    if ((pEl.text().contains("Soggetto:") || pEl.text().contains("soggetto: ")) &&
                            pEl.text().contains("Con la collaborazione di")) {
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                        toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.script));
                        toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.script));
                        toRet.addAll(getAuthors(authorService, genericEl.get(3).text(), AuthorRole.illustration));
                        toRet.addAll(getAuthors(authorService, genericEl.get(4).text(), AuthorRole.cover)); 
                    } else if(pEl.text().contains("Soggetto e sceneggiatura: ") && 
                            pEl.text().contains("Disegni: ") &&
                            pEl.text().contains("Copertina: ") &&
                            pEl.text().contains("/")) {
                        if(pEl.text().indexOf("/") > pEl.text().indexOf("Disegni")) {
                            toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                            toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.script));
                            toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.illustration));                            
                            toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.illustration));                            
                            toRet.addAll(getAuthors(authorService, genericEl.get(3).text(), AuthorRole.cover)); 
                        } else {
                            toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                            toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.script));
                            toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.subject));
                            toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.script));
                            toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.illustration));
                            toRet.addAll(getAuthors(authorService, genericEl.get(3).text(), AuthorRole.illustration));
                            toRet.addAll(getAuthors(authorService, genericEl.get(4).text(), AuthorRole.cover)); 
                        }
                    } else if(pEl.text().contains("Soggetto e sceneggiatura: ") && 
                            pEl.text().contains("Disegni: ") &&
                            pEl.text().contains("Copertina: ") &&
                            pEl.text().contains(" e ") &&
                            !pEl.text().contains(" (Gilbert) ")) {
                        if(pEl.text().lastIndexOf(" e ") > pEl.text().indexOf("Disegni")) {
                            toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                            toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.script));
                            toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.illustration));                            
                            toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.illustration));                            
                            toRet.addAll(getAuthors(authorService, genericEl.get(3).text(), AuthorRole.cover)); 
                        } else {
                            toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                            toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.script));
                            toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.subject));
                            toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.script));
                            toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.illustration));
                            toRet.addAll(getAuthors(authorService, genericEl.get(3).text(), AuthorRole.illustration));
                            toRet.addAll(getAuthors(authorService, genericEl.get(4).text(), AuthorRole.cover)); 
                        }
                    }else if(pEl.text().contains("Soggetto e sceneggiatura: ") && 
                            pEl.text().contains("Disegni: ") &&
                            pEl.text().contains("Copertina: ") &&
                            pEl.text().contains(" (Gilbert) ")) {
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.script));
                        toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.illustration));                            
                        toRet.addAll(getAuthors(authorService, genericEl.get(3).text(), AuthorRole.illustration));                            
                        toRet.addAll(getAuthors(authorService, genericEl.get(4).text(), AuthorRole.cover)); 
                    }else if((pEl.text().contains("Soggetto:") || pEl.text().contains("soggetto: ")) &&
                            (pEl.text().contains("Sceneggiatura:") || pEl.text().contains("sceneggiatura: ")) &&
                            pEl.text().contains("Disegni") &&
                            pEl.text().contains("Copertina")) {
                        if(pEl.text().contains(" e ")) {
                            if(pEl.text().indexOf(" e ") > pEl.text().indexOf("Disegni")) {
                                toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                                toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.script));
                                toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.illustration));                            
                                toRet.addAll(getAuthors(authorService, genericEl.get(3).text(), AuthorRole.illustration));                            
                                toRet.addAll(getAuthors(authorService, genericEl.get(4).text(), AuthorRole.cover)); 
                            } else if(pEl.text().indexOf(" e ") > pEl.text().indexOf("ceneggiatura")) {
                                toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                                toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.script));
                                toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.script));                            
                                toRet.addAll(getAuthors(authorService, genericEl.get(3).text(), AuthorRole.illustration));                            
                                toRet.addAll(getAuthors(authorService, genericEl.get(4).text(), AuthorRole.cover)); 
                            } else if(pEl.text().indexOf(" e ") > pEl.text().indexOf("oggetto")) {
                                toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                                toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.subject));
                                toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.script));                            
                                toRet.addAll(getAuthors(authorService, genericEl.get(3).text(), AuthorRole.illustration));                            
                                toRet.addAll(getAuthors(authorService, genericEl.get(4).text(), AuthorRole.cover)); 
                            }
                        } else {
                            System.out.println("issue");
                        }
                    }
                    break;
                case 6: 
                    if (pEl.text().contains("Soggetto e sceneggiatura: ")
                            && pEl.text().contains("Disegni: ")
                            && pEl.text().contains("Copertina: ")
                            && pEl.text().contains("/") 
                            && pEl.text().contains("In questo numero:")) {                        
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.subject));
                        toRet.addAll(getAuthors(authorService, genericEl.get(0).text(), AuthorRole.script));
                        toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.subject));
                        toRet.addAll(getAuthors(authorService, genericEl.get(1).text(), AuthorRole.script));
                        toRet.addAll(getAuthors(authorService, genericEl.get(2).text(), AuthorRole.illustration));
                        toRet.addAll(getAuthors(authorService, genericEl.get(3).text(), AuthorRole.illustration));
                        toRet.addAll(getAuthors(authorService, genericEl.get(4).text(), AuthorRole.cover));                                                
                    } else {
                        System.out.println("issue");
                    }
                    break;
                default:
                    break;
            }
        }
        return toRet;
    }
}
