package it.manca.bonelli.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author marcomanca
 */
@Entity
@Table(name = "Tag")
public class Tag {
    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private Long tagId;
    private String tagName;
    private String tagLink;
    @JsonIgnore
    @ManyToMany(mappedBy = "tags",
            fetch = FetchType.LAZY)
    private List<Pubblication> publications = new LinkedList<>();
    
    public Tag() {
    }

    public Tag(String tagName, String tagLink) {
        this.tagName = tagName;
        this.tagLink = tagLink;
    }

//    public Long getTagId() {
//        return tagId;
//    }
//
//    public void setTagId(Long tagId) {
//        this.tagId = tagId;
//    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getTagLink() {
        return tagLink;
    }

    public void setTagLink(String tagLink) {
        this.tagLink = tagLink;
    }

    public List<Pubblication> getPublications() {
        return publications;
    }

    public void setPublications(List<Pubblication> publications) {
        this.publications = publications;
    }
    
    
}
