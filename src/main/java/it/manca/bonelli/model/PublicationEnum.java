package it.manca.bonelli.model;

/**
 *
 * @author marcomanca
 */
public enum PublicationEnum {
    dylandog, colordylandog, julia, tex, colortex, morganlost, orfanisam, nathannever, zagor, martinmystere
}
