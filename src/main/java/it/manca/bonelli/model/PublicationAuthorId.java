package it.manca.bonelli.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

/**
 *
 * @author Marco Manca
 */
@Embeddable
public class PublicationAuthorId  implements Serializable{
    @JsonIgnore
    private Pubblication pubblication;
    private Author author;
    private AuthorRole role;
    
    @ManyToOne(fetch = FetchType.LAZY)
    public Pubblication getPubblication() {
        return pubblication;
    }

    public void setPubblication(Pubblication pubblication) {
        this.pubblication = pubblication;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public AuthorRole getRole() {
        return role;
    }

    public void setRole(AuthorRole role) {
        this.role = role;
    }
    
    @Override
    public int hashCode() {
        int result;
        result = (pubblication != null ? pubblication.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PublicationAuthorId other = (PublicationAuthorId) obj;
        if (!Objects.equals(this.pubblication, other.pubblication)) {
            return false;
        }
        if (!Objects.equals(this.author, other.author)) {
            return false;
        }
        return true;
    }
    
    
    
}
