package it.manca.bonelli.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;

/**
 *
 * @author marcomanca
 */
@Embeddable
public class PubblicationId implements Serializable{
    private int number;
    private String pubblicationName;

    public PubblicationId() {
    }

    public PubblicationId(int number, String pubblicationName) {
        this.number = number;
        this.pubblicationName = pubblicationName;
    }

    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + this.number;
        hash = 11 * hash + Objects.hashCode(this.pubblicationName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PubblicationId other = (PubblicationId) obj;
        if (this.number != other.number) {
            return false;
        }
        if (!Objects.equals(this.pubblicationName, other.pubblicationName)) {
            return false;
        }
        return true;
    }
    
    
}
