package it.manca.bonelli.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author marcomanca
 */
@Entity
@Table(name = "Author")
public class Author implements Serializable  {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long authorId;
    private String name;
    private String surname;
    private String nameSurname;
        
    /*https://mkyong.com/hibernate/hibernate-many-to-many-example-join-table-extra-column-annotation/*/
    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "pk.author")
    private List<PubblicationAuthor> pubblicationAuthors; 
    
    public Author() {
        this.pubblicationAuthors = new LinkedList<>();
    }

    public Author(String surname) {
        this.name = "";
        this.nameSurname = surname;
        this.surname = surname;
        this.pubblicationAuthors = new LinkedList<>();
    }
        
    public Author(String name, String surname) {
        this.name = name;
        this.surname = surname;     
        this.nameSurname = name.concat(" ").concat(surname);
        this.pubblicationAuthors = new LinkedList<>();
    }
    
    public Author(String name, String surname, String nameSurname) {
        this.name = name;
        this.surname = surname;
        this.nameSurname = nameSurname;        
        this.pubblicationAuthors = new LinkedList<>();
    }

        
    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }
    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
    
    public String getNameSurname() {
        return nameSurname;
    }

    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    public List<PubblicationAuthor> getPubblicationAuthors() {
        return pubblicationAuthors;
    }

    public void setPubblicationAuthors(List<PubblicationAuthor> pubblicationAuthors) {
        this.pubblicationAuthors = pubblicationAuthors;
    }

    
}
