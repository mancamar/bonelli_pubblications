package it.manca.bonelli.model;

import java.util.List;

/**
 *
 * @author Marco Manca
 */
public class PubblicationUpdates {
    private final List<Pubblication> newPubblication;
    private final List<Pubblication> oldPubblication;

    public PubblicationUpdates(List<Pubblication> newPubblication, List<Pubblication> oldPubblication) {
        this.newPubblication = newPubblication;
        this.oldPubblication = oldPubblication;
    }

    public List<Pubblication> getNewPubblication() {
        return newPubblication;
    }

    public List<Pubblication> getOldPubblication() {
        return oldPubblication;
    }
        
}
