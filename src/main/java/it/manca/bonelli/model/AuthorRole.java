package it.manca.bonelli.model;

/**
 *
 * @author marcomanca
 */
public enum AuthorRole {
    subject, script, illustration, color, cover, cover_color, unknown
}
