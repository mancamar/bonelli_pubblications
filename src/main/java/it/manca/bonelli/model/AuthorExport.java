package it.manca.bonelli.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marco Manca
 */
public class AuthorExport {
    private String id;
    private Name name;

    public AuthorExport() {
    }

    public AuthorExport(String nameSurname, String name, String surname) {
        this.id = surname;
        List<String> synonyms = new ArrayList<>(2);        
        if(!"".equals(name) && !name.concat(" ").concat(surname).equals(nameSurname)) {
            synonyms.add(name.concat(" ").concat(surname));
        }
        synonyms.add(nameSurname);
        this.name = new Name(surname, synonyms);
    }

    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }
    
    private static class Name {
        private String value;
        private List<String> synonyms;
        
        public Name() {
        }

        public Name(String value, List<String> synonyms) {
            this.value = value;
            this.synonyms = synonyms;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public List<String> getSynonyms() {
            return synonyms;
        }

        public void setSynonyms(List<String> synonyms) {
            this.synonyms = synonyms;
        }
                
        
    }
    
}
