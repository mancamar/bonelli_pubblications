package it.manca.bonelli.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Marco Manca
 */
@Entity
@Table(name="pubblication_author")
/*https://mkyong.com/hibernate/hibernate-many-to-many-example-join-table-extra-column-annotation/*/
@AssociationOverrides({
    @AssociationOverride(name = "pk.pubblication", 
	joinColumns = {
            @JoinColumn(name="number", nullable = false, insertable = true, updatable = true), 
            @JoinColumn(name="pubblicationName", nullable = false, insertable = true, updatable = true)
        }),
    @AssociationOverride(name = "pk.author", 
        joinColumns = @JoinColumn(name = "authorId")) 
})
public class PubblicationAuthor implements Serializable {
    
    private PublicationAuthorId pk;
        
    

    public PubblicationAuthor() {
        pk = new PublicationAuthorId();
    }
    
    public PubblicationAuthor(Author author, AuthorRole role) {
        pk = new PublicationAuthorId();
        getPk().setAuthor(author);
        getPk().setRole(role);
    }
    
    @EmbeddedId
    public PublicationAuthorId getPk() {
        return pk;
    }

    public void setPk(PublicationAuthorId pk) {
        this.pk = pk;
    }

    @Transient
    public Pubblication getPubblication() {
        return getPk().getPubblication();
    }
    
    public void setPubblication(Pubblication p) {
        getPk().setPubblication(p);
    }
    
    @Transient
    public Author getAuthor() {
        return getPk().getAuthor();
    }
    
    public void setAuthor(Author a) {
        getPk().setAuthor(a);
    }
    
    @Override
    public int hashCode() {
        return (getPk() != null ? getPk().hashCode() : 0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PubblicationAuthor that = (PubblicationAuthor) o;

        if (getPk() != null ? !getPk().equals(that.getPk())
                : that.getPk() != null) {
            return false;
        }

        return true;
    }
    
    
}
