package it.manca.bonelli.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 *
 * @author munky
 */
@Entity
@Table(name="Pubblication")
@IdClass(PubblicationId.class)
public class Pubblication implements Serializable {
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private Long pubblicationId;
    @Column(nullable = false)
    @Temporal(javax.persistence.TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date pubblicationDate;
    @Id
    private int number;
    @Column(nullable = false)
    private String periodicity;
    @Id
    private String pubblicationName;
    @Column(nullable = false)
    private String pubblicationTitle;
    @Column(nullable = false)
    private String smallCoverURL;
    @Column(nullable = false)
    private String bigCoverURL;
    @Column(nullable = false)
    private String schedaURL;
    /*
    Modifica 15-03-2020
    @OneToMany(targetEntity = Author.class, cascade = {
        CascadeType.ALL},            
            orphanRemoval = true,
            fetch = FetchType.EAGER)
    @JoinColumns(
            {
        @JoinColumn(name="number", nullable = false, insertable = true, updatable = true),
        @JoinColumn(name="pubblicationName", nullable = false, insertable = true, updatable = true)
            })  
    */ 
    /*https://mkyong.com/hibernate/hibernate-many-to-many-example-join-table-extra-column-annotation/*/
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "pk.pubblication", cascade = { 
        CascadeType.ALL
    })    
    private List<PubblicationAuthor> pubblicationAuthors;
    
    @ManyToMany(cascade = { 
        CascadeType.ALL        
    })
    @JoinTable(name = "pubblication_tag",        
        joinColumns = {@JoinColumn(name = "number"),@JoinColumn(name = "pubblicationName")},
        //mod 15-03 inverseJoinColumns = @JoinColumn(name = "tagId")
        inverseJoinColumns = @JoinColumn(name = "tagName")
    )
    @JsonIgnore
    private List<Tag> tags;
    @Column(length=100000)
    @Lob
    private String summary;
    private boolean currentInNewsstand;
    @JsonIgnore
    @Transient 
    private String previousPubblicationURL;
    
    public Pubblication() {
        this.currentInNewsstand = false;
        this.tags = new LinkedList<>();
    }
        
    public Pubblication(Date pubblicationDate, int number, String periodicity, 
            String pubblicationName, String pubblicationTitle, String bigCoverURL, 
            String summary, String prevPubblicationUrl) {
        this.pubblicationDate = pubblicationDate;
        this.number = number;
        this.periodicity = periodicity;
        this.pubblicationName = pubblicationName;
        this.pubblicationTitle = pubblicationTitle;
        this.bigCoverURL = bigCoverURL;        
        this.summary = summary;
        this.currentInNewsstand = false;
        this.previousPubblicationURL = prevPubblicationUrl;
        this.tags = new LinkedList<>();
    }

    public Pubblication(Date pubblicationDate, int number, String periodicity, 
            String pubblicationName, String pubblicationTitle,
            String bigCoverURL, String schedaURL,
            String summary, String previousPubblicationURL) {
        this.pubblicationDate = pubblicationDate;
        this.number = number;
        this.periodicity = periodicity;
        this.pubblicationName = pubblicationName;
        this.pubblicationTitle = pubblicationTitle;
        this.bigCoverURL = bigCoverURL;
        this.schedaURL = schedaURL;        
        this.summary = summary;
        this.previousPubblicationURL = previousPubblicationURL;
        this.tags = new LinkedList<>();                
    }

    public Pubblication(Date pubblicationDate, int pubblicationNumber, 
            String pubblicationPeriodicity, String pubblicationName, 
            String pubblicationTitle, String smallCoverUrl, String bigCoverURL, 
            String pubblicationSchedaURL, String summary, 
            String prevPubblicationUrl, List<Tag> tagList) {
        this.pubblicationDate = pubblicationDate;
        this.number = pubblicationNumber;
        this.periodicity = pubblicationPeriodicity;
        this.pubblicationName = pubblicationName;
        this.pubblicationTitle = pubblicationTitle;
        this.smallCoverURL = smallCoverUrl;
        this.bigCoverURL = bigCoverURL;
        this.schedaURL = pubblicationSchedaURL;        
        this.summary = summary;
        this.previousPubblicationURL = prevPubblicationUrl;
        this.tags = tagList;
        this.currentInNewsstand = false;
    }
  
    
    @Override
    public String toString() {
        return "Name:".concat(pubblicationName).concat(" num: ")
                .concat(Integer.toString(number)).concat(" title: ")
                .concat(pubblicationTitle).concat(" date: ")
                .concat(pubblicationDate.toString());
    }
    
    public Date getPubblicationDate() {
        return pubblicationDate;
    }

    public void setPubblicationDate(Date pubblicationDate) {
        this.pubblicationDate = pubblicationDate;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(String periodicity) {
        this.periodicity = periodicity;
    }

    public String getPubblicationName() {
        return pubblicationName;
    }

    public void setPubblicationName(String pubblicationName) {
        this.pubblicationName = pubblicationName;
    }

    public String getPubblicationTitle() {
        return pubblicationTitle;
    }

    public void setPubblicationTitle(String pubblicationTitle) {
        this.pubblicationTitle = pubblicationTitle;
    }

    public String getSmallCoverURL() {
        return smallCoverURL;
    }

    public void setSmallCoverURL(String smallCoverURL) {
        this.smallCoverURL = smallCoverURL;
    }

    public String getBigCoverURL() {
        return bigCoverURL;
    }

    public void setBigCoverURL(String bigCoverURL) {
        this.bigCoverURL = bigCoverURL;
    }

    
    public String getSchedaURL() {
        return schedaURL;
    }

    public void setSchedaURL(String schedaURL) {
        this.schedaURL = schedaURL;
    }
  
    @Override
    public boolean equals(Object  o) {
        if(!o.getClass().equals(Pubblication.class))
            return false;
        Pubblication p = (Pubblication)o;
        return this.getPubblicationName().equals(p.getPubblicationName()) &&
                this.getNumber() == p.getNumber();
    }

    public List<PubblicationAuthor> getPubblicationAuthors() {
        if(pubblicationAuthors == null) {
            pubblicationAuthors = new LinkedList<>();
        }
        return pubblicationAuthors;
    }

    public void setPubblicationAuthors(List<PubblicationAuthor> pubblicationAuthors) {
        this.pubblicationAuthors = pubblicationAuthors;
    }
        
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public boolean isCurrentInNewsstand() {
        return currentInNewsstand;
    }

    public void setCurrentInNewsstand(boolean currentInNewsstand) {
        this.currentInNewsstand = currentInNewsstand;
    }

    public String getPreviousPubblicationURL() {
        return previousPubblicationURL;
    }

    public void setPreviousPubblicationURL(String previousPubblicationURL) {
        this.previousPubblicationURL = previousPubblicationURL;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.number;
        hash = 89 * hash + Objects.hashCode(this.pubblicationName);
        return hash;
    }
}


