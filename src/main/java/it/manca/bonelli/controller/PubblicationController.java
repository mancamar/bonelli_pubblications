package it.manca.bonelli.controller;

import it.manca.bonelli.model.Author;
import it.manca.bonelli.model.AuthorExport;
import it.manca.bonelli.model.Pubblication;
import it.manca.bonelli.model.PubblicationUpdates;
import it.manca.bonelli.model.PublicationEnum;
import it.manca.bonelli.service.AuthorService;
import it.manca.bonelli.utils.PubblicationUtils;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import it.manca.bonelli.service.PublicationService;

/**
 *
 * @author munky
 */
@Controller
@RequestMapping("/rest")
public class PubblicationController {

    @Autowired
    private PublicationService publicationService;
    @Autowired
    private AuthorService authorService;
    @Autowired
    private String nextPublicationsURL;
    @Autowired
    private String currentPublicationsURL;
    @Autowired
    private String shopBaseUrl;
    @Autowired
    private String dylanDogPublicationsURL;
    @Autowired
    private String colorDylanDogPublicationsURL;
    @Autowired
    private String colorTexUrl;
    @Autowired
    private String juliaUrl;
    @Autowired
    private String orfaniSamUrl;
    @Autowired
    private String nathanUrl;
    @Autowired
    private String zagorUrl;
    @Autowired
    private String texUrl;
    @Autowired
    private String martinMystereUrl;

    /* START COUNT */
    @RequestMapping(value = {"/countPublications/authorSurname/{authorSurname}",
        "/countPublications/authorSurname/{authorSurname}/year/{year}"}, 
            method = RequestMethod.GET, 
            produces = "application/json")
    public @ResponseBody
    int countPublicationsByAuthorSurname(@PathVariable String authorSurname,
            @PathVariable Optional<Integer> year) {
        if(year.isPresent()) {                            
            return publicationService.countPublicationsByYearAndAuthorSurname(year.get(), authorSurname);            
        } else {
            return publicationService.countPublicationsByAuthorSurname(authorSurname);
        }        
    }
            
    @RequestMapping(value = {"/countPublications/authorNameSurname/{authorNameSurname}",
        "/countPublications/authorNameSurname/{authorNameSurname}/year/{year}"}, 
            method = RequestMethod.GET, 
            produces = "application/json")
    public @ResponseBody
    int countPublicationsByAuthorNameSurname(@PathVariable String authorNameSurname,
            @PathVariable Optional<Integer> year) {
        if(year.isPresent()) {
            if(year.get() > 0 && year.get() < 1950) {
                return 0;
            } else {                
                return publicationService.countPublicationsByYearAndAuthorNameSurname(year.get(), authorNameSurname);
            }                    
        } else {
            return publicationService.countPublicationsByAuthorNameSurname(authorNameSurname);
        } 
    }
    
    @RequestMapping(value = {"/countPublications/pubblicationName/{pubblicationName}/authorSurname/{authorSurname}",
        "/countPublications/pubblicationName/{pubblicationName}/authorSurname/{authorSurname}/year/{year}"}, 
            method = RequestMethod.GET, 
            produces = "application/json")
    public @ResponseBody
    int countPublicationsByComicaNameAndAuthorSurname(@PathVariable String pubblicationName, 
            @PathVariable String authorSurname,
            @PathVariable Optional<Integer> year) {
        if(year.isPresent()) {
            if(year.get() > 0 && year.get() < 1950) {
                return 0;
            } else {                
                return publicationService.countPublicationsByComicNameAndYearAndAuthorSurname(pubblicationName, year.get(), authorSurname);
            }                    
        } else {
            return publicationService.countPublicationsByComicNameAndAuthorSurname(pubblicationName, authorSurname);
        } 
    }
    
    @RequestMapping(value = {"/countPublications/pubblicationName/{pubblicationName}/authorNameSurname/{authorNameSurname}",
        "/countPublications/pubblicationName/{pubblicationName}/authorNameSurname/{authorNameSurname}/year/{year}"}, 
            method = RequestMethod.GET, 
            produces = "application/json")
    public @ResponseBody
    int countPublicationsByComicaNameAndAuthorNameSurname(@PathVariable String pubblicationName, 
            @PathVariable String authorNameSurname,
            @PathVariable(required = false) Optional<Integer> year) {
        if(year.isPresent()) {
            if(year.get() > 0 && year.get() < 1950) {
                return 0;
            } else {               
                return publicationService.countPublicationsByNameAndYearAndAuthorNameSurname(pubblicationName, year.get(), authorNameSurname);
            }                    
        } else {
            return publicationService.countPublicationsByNameAndAuthorNameSurname(pubblicationName, authorNameSurname);
        } 
    }
      
    /* END COUNT */
    
    /* START IN NEWSSTAND */
    @RequestMapping(value = "/findPublicationInNewsstand", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getCurrentPublicationInNewsstand() {
        List<Pubblication> dbPublicationList = publicationService.findPublicationsCurrentInNewsstand();
        return dbPublicationList;
    }
    
    @RequestMapping(value = "/findPublicationInNewsstand/authorName/{authorName}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getCurrentPublicationInNewsstandByAuthorName(@PathVariable String authorName) {
        List<Pubblication> dbPublicationList = publicationService.findPublicationsCurrentInNewsstandByAuthorName(authorName);
        return dbPublicationList;
    }
    
    @RequestMapping(value = "/findPublicationInNewsstand/authorSurname/{authorSurname}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getCurrentPublicationInNewsstandByAuthorSurname(@PathVariable String authorSurname) {
        List<Pubblication> dbPublicationList = publicationService.findPublicationsCurrentInNewsstandByAuthorSurname(authorSurname);
        return dbPublicationList;
    }
   
    @RequestMapping(value = "/findPublicationInNewsstand/authorNameSurname/{authorNameSurname}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getCurrentPublicationInNewsstandByAuthorNameSurname(@PathVariable String authorNameSurname) {
        List<Pubblication> dbPublicationList = publicationService.findPublicationsCurrentInNewsstandByAuthorNameSurname(authorNameSurname);
        return dbPublicationList;
    }
    @RequestMapping(value = "/findPublicationInNewsstand/pubblicationName/{pubblicationName}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getCurrentPublicationInNewsstandByPublicationName(@PathVariable String pubblicationName) {
        List<Pubblication> dbPublicationList = publicationService.findPublicationsCurrentInNewsstandByPubblicationName(pubblicationName);
        return dbPublicationList;
    }
    
    @RequestMapping(value = "/findPublicationInNewsstand/pubblicationName/{pubblicationName}/authorSurname/{authorSurname}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getCurrentPublicationInNewsstandByPublicationNameAndAuthorSurname(@PathVariable String pubblicationName, @PathVariable String authorSurname) {
        List<Pubblication> dbPublicationList = publicationService.findPublicationsCurrentInNewsstandByComicNameAndAuthorSurname(pubblicationName, authorSurname);
        return dbPublicationList;
    }
    
    @RequestMapping(value = "/findPublicationInNewsstand/pubblicationName/{pubblicationName}/authorNameSurname/{authorNameSurname}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getCurrentPublicationInNewsstandByPublicationNameAndAuthorNameSurname(@PathVariable String pubblicationName, @PathVariable String authorNameSurname) {
        List<Pubblication> dbPublicationList = publicationService.findPublicationsCurrentInNewsstandByComicNameAndAuthorNameSurname(pubblicationName, authorNameSurname);
        return dbPublicationList;
    }
    @RequestMapping(value = "/findPublicationInNewsstand/timeInterval/{timeInterval}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getNextPublicationsByTimeInterval(@PathVariable String timeInterval) {
        Date startDate = null, endDate = null;
        Calendar calendarToday = Calendar.getInstance(TimeZone.getTimeZone(ZoneId.of("Europe/Rome")));
                
        switch(timeInterval) {
            case "settimana":
                switch(calendarToday.getFirstDayOfWeek()) {
            case Calendar.MONDAY:
                startDate = Date.from(Instant.now());        
                endDate = Date.from(Instant.now().plus(6, ChronoUnit.DAYS));        
                break;
            case Calendar.TUESDAY:
                startDate = Date.from(Instant.now().minus(1, ChronoUnit.DAYS));        
                endDate = Date.from(Instant.now().plus(5, ChronoUnit.DAYS));        
                break;
            case Calendar.WEDNESDAY:
                startDate = Date.from(Instant.now().minus(2, ChronoUnit.DAYS));        
                endDate = Date.from(Instant.now().plus(4, ChronoUnit.DAYS));        
                break;
            case Calendar.THURSDAY:
                startDate = Date.from(Instant.now().minus(3, ChronoUnit.DAYS));        
                endDate = Date.from(Instant.now().plus(3, ChronoUnit.DAYS));        
                break;
            case Calendar.FRIDAY:
                startDate = Date.from(Instant.now().minus(4, ChronoUnit.DAYS));        
                endDate = Date.from(Instant.now().plus(2, ChronoUnit.DAYS));        
                break;
            case Calendar.SATURDAY:
                startDate = Date.from(Instant.now().minus(5, ChronoUnit.DAYS));        
                endDate = Date.from(Instant.now().plus(1, ChronoUnit.DAYS));        
                break;
            case Calendar.SUNDAY:
                startDate = Date.from(Instant.now().plus(1, ChronoUnit.DAYS));        
                endDate = Date.from(Instant.now().plus(7, ChronoUnit.DAYS));        
                break;
        }
                break;
            case "mese":
                Calendar calendarStart = Calendar.getInstance();
                calendarStart.set(calendarStart.get(Calendar.YEAR), calendarStart.get(Calendar.MONTH), 01, 00, 00, 01);
                startDate = calendarStart.getTime();
                
                LocalDate now = LocalDate.now();                                
                endDate = Date.from(now.withDayOfMonth(now.getMonth().length(now.isLeapYear())).atStartOfDay(ZoneId.of("Europe/Rome")).toInstant());
                break;
            default:
                return publicationService.findNextPublications();                            
        }
        return publicationService.findPublicationsCurrentInNewsstandByTimeInterval(startDate, endDate);
    }
    /* END IN NEWSSTAND */
    
    /* START NEXT PUBBLICATIONS */
    @RequestMapping(value = "/findNextPublications", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getNextPublications() {
        List<Pubblication> dbPublicationList = publicationService.findNextPublications();
        return dbPublicationList;
    }
    
    @RequestMapping(value = "/findNextPublications/authorName/{authorName}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getNextPublicationsByAuthorName(@PathVariable String authorName) {
        List<Pubblication> dbPublicationList = publicationService.findNextPublicationsByAuthorName(authorName);
        return dbPublicationList;
    }
    
    @RequestMapping(value = "/findNextPublications/authorSurname/{authorSurname}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getNextPublicationsByAuthorSurname(@PathVariable String authorSurname) {
        List<Pubblication> dbPublicationList = publicationService.findNextPublicationsByAuthorSurname(authorSurname);
        return dbPublicationList;
    }
   
    @RequestMapping(value = "/findNextPublications/authorNameSurname/{authorNameSurname}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getNextPublicationsByAuthorNameSurname(@PathVariable String authorNameSurname) {
        List<Pubblication> dbPublicationList = publicationService.findNextPublicationsByAuthorNameSurname(authorNameSurname);
        return dbPublicationList;
    }
    @RequestMapping(value = "/findNextPublications/pubblicationName/{pubblicationName}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getNextPublicationsByPublicationName(@PathVariable String pubblicationName) {
        List<Pubblication> dbPublicationList = publicationService.findNextPublicationsByComicName(pubblicationName);
        return dbPublicationList;
    }
    
    @RequestMapping(value = "/findNextPublications/pubblicationName/{pubblicationName}/authorSurname/{authorSurname}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getNextPublicationsByPubblicationNameAndAuthorSurname(@PathVariable String pubblicationName, @PathVariable String authorSurname) {
        List<Pubblication> dbPublicationList = publicationService.findNextPublicationsByComicNameAndAuthorSurname(pubblicationName, authorSurname);
        return dbPublicationList;
    }
    
    @RequestMapping(value = "/findNextPublications/pubblicationName/{pubblicationName}/authorNameSurname/{authorNameSurname}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getNextPublicationsByPublicationNameAndAuthorNameSurname(@PathVariable String pubblicationName, @PathVariable String authorNameSurname) {
        List<Pubblication> dbPublicationList = publicationService.findNextPublicationsByComicNameAndAuthorNameSurname(pubblicationName, authorNameSurname);
        return dbPublicationList;
    }        
    /* END NEXT PUBBLICATIONS */
    
    /* START BY YEAR*/    
    @RequestMapping(value = "/findPublications/pubblicationName/{pubblicationName}/year/{year}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> findPublicationsByYear(@PathVariable String pubblicationName, @PathVariable int year) {        
        return publicationService.findPublicationsByYearAndComicName(year, pubblicationName);
    }
    @RequestMapping(value = "/findPublications/authorSurname/{authorSurname}/year/{year}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> findPublicationsByYearAndAuthorSurname(@PathVariable String authorSurname, @PathVariable int year) {        
        return publicationService.findPublicationsByYearAndAuthorSurname(year, authorSurname);
    }
    @RequestMapping(value = "/findPublications/authorNameSurname/{authorNameSurname}/year/{year}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> findPublicationsByYearAndAuthorNameSurname(@PathVariable String authorNameSurname, @PathVariable int year) {        
        return publicationService.findPublicationsByYearAndAuthorNameSurname(year, authorNameSurname);
    }    
    /* END BY YEAR*/    

//    @RequestMapping(value = "/findAll", method = RequestMethod.GET, produces = "application/json")
//    public @ResponseBody
//    List<Pubblication> getAllPublication() {
//        List<Pubblication> dbPublicationList = pubblicationService.findAllPublications();
//        return dbPublicationList;
//    }
    /* START BY COMIC NAME */    
    @RequestMapping(value = "/findPublication/pubblicationName/{pubblicationName}/pubblicationNumber/{pubblicationNumber}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Pubblication getPublicationsByNameAndNumber(@PathVariable String pubblicationName, @PathVariable int pubblicationNumber) {
        return publicationService.findPublicationByComicNameAndNumber(pubblicationName, pubblicationNumber);        
    }
    /* END BY COMIC NAME */    
    
    /* START BY AUTHOR NAME */    
    @RequestMapping(value = "/findPublications/authorName/{authorName}/authorSurname/{authorSurname}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getPublicationsByAuthorNameAndSurname(@PathVariable String authorName,
            @PathVariable String authorSurname) {
        List<Pubblication> dbPublicationList = publicationService.findPublicationsByAuthorNameSurname(authorName.concat(" ").concat(authorSurname));
        return dbPublicationList;
    }
    
    @RequestMapping(value = "/findPublications/authorSurname/{authorSurname}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getPublicationsByAuthorSurname(@PathVariable String authorSurname) {
        List<Pubblication> dbPublicationList = publicationService.findPublicationsByAuthorSurname(authorSurname);
        return dbPublicationList;
    }
    
    @RequestMapping(value = "/findPublications/authorNameSurname/{nameSurname}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getPublicationsByAuthorNameSurname(@PathVariable String nameSurname) {
        List<Pubblication> dbPublicationList = publicationService.findPublicationsByAuthorNameSurname(nameSurname);
        return dbPublicationList;
    }
    
    @RequestMapping(value = "/findPublications/authorSurname/{surname}/pubblicationName/{pubblicationName}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getPublicationsByAuthorSurnameAndPublicationName(@PathVariable String surname, @PathVariable String pubblicationName) {
        List<Pubblication> dbPublicationList = publicationService.findPublicationsByComicNameAndAuthorSurname(pubblicationName, surname);
        return dbPublicationList;
    }
    
    @RequestMapping(value = "/findPublications/authorNameSurname/{nameSurname}/pubblicationName/{pubblicationName}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getPublicationsByAuthorNameSurnameAndPublicationName(@PathVariable String nameSurname, @PathVariable String pubblicationName) {
        List<Pubblication> dbPublicationList = publicationService.findPublicationsByComicNameAndAuthorNameSurname(pubblicationName, nameSurname);
        return dbPublicationList;
    }
    
    @RequestMapping(value = "/findLastPublication/authorSurname/{surname}/pubblicationName/{pubblicationName}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Pubblication getLastPublicationByAuthorSurnameAndPublicationName(@PathVariable String surname, @PathVariable String pubblicationName) {
        Pubblication pub = publicationService.findLastPubblicationByComicNameAndAuthorSurname(pubblicationName, surname);
        return pub;
    }
    
    @RequestMapping(value = "/findPublication/authorSurname/{surname}/pubblicationName/{pubblicationName}/ordinal/{ordinal}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Pubblication getPublicationByOrdinalAndAuthorSurnameAndPublicationName(@PathVariable String surname, 
            @PathVariable String pubblicationName, @PathVariable int ordinal) {
        Pubblication pub = publicationService.findPubblicationByOrdinalAndComicNameAndAuthorSurname(ordinal, pubblicationName, surname);
        return pub;
    }
    
    @RequestMapping(value = "/findLastPublication/authorNameSurname/{nameSurname}/pubblicationName/{pubblicationName}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Pubblication getLastPublicationByAuthorNameSurnameAndPublicationName(@PathVariable String nameSurname, @PathVariable String pubblicationName) {
        Pubblication pub = publicationService.findLastPubblicationByComicNameAndAuthorNameSurname(pubblicationName, nameSurname);
        return pub;
    }
    
    @RequestMapping(value = "/findPublication/authorNameSurname/{nameSurname}/pubblicationName/{pubblicationName}/ordinal/{ordinal}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Pubblication getPublicationByOrdinalAndAuthorNameSurnameAndPublicationName(@PathVariable String nameSurname,
            @PathVariable String pubblicationName, @PathVariable int ordinal) {
        Pubblication pub = publicationService.findPubblicationByOrdinalAndComicNameAndAuthorNameSurname(ordinal, pubblicationName, nameSurname);
        return pub;
    }
    
    @RequestMapping(value = "/findLastPublication/authorSurname/{surname}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Pubblication getLastPublicationByAuthorSurname(@PathVariable String surname) {
        Pubblication pub = publicationService.findLastPubblicationByAuthorSurname(surname);
        return pub;
    }
    
    @RequestMapping(value = "/findPublication/authorSurname/{surname}/ordinal/{ordinal}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Pubblication getPublicationByOrdinalAuthorSurname(@PathVariable String surname,
            @PathVariable int ordinal) {
        Pubblication pub = publicationService.findPubblicationByOrdinalAndAuthorSurname(ordinal, surname);
        return pub;
    }
    
    @RequestMapping(value = "/findLastPublication/authorNameSurname/{nameSurname}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Pubblication getLastPublicationByAuthorNameSurname(@PathVariable String nameSurname) {
        Pubblication pub = publicationService.findLastPubblicationByAuthorNameSurname(nameSurname);
        return pub;
    }
    
    @RequestMapping(value = "/findPublication/authorNameSurname/{nameSurname}/ordinal/{ordinal}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Pubblication getLastPublicationByAuthorNameSurname(@PathVariable String nameSurname,
            @PathVariable int ordinal) {
        Pubblication pub = publicationService.findPubblicationByOrdinalAndAuthorNameSurname(ordinal, nameSurname);
        return pub;
    }
    
    @RequestMapping(value = "/findPublications/authorSurname/{surname}/pubblicationName/{pubblicationName}/year/{year}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getPublicationsByAuthorSurnameAndPublicationNameAndYear(@PathVariable String surname, 
            @PathVariable String pubblicationName, @PathVariable int year) {
        List<Pubblication> dbPublicationList = publicationService.findPublicationsByComicNameAndYearAndAuthorSurname(pubblicationName,year, surname);
        return dbPublicationList;
    }
    
    @RequestMapping(value = "/findPublications/authorNameSurname/{nameSurname}/pubblicationName/{pubblicationName}/year/{year}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getPublicationsByAuthorNameSurnameAndPublicationNameAndYear(@PathVariable String nameSurname, 
            @PathVariable String pubblicationName, @PathVariable int year) {
        List<Pubblication> dbPublicationList = publicationService.findPublicationsByComicNameAndYearAndAuthorNameSurname(pubblicationName, year, nameSurname);
        return dbPublicationList;
    }
    /* END BY AUTHOR NAME */    
    
    @RequestMapping(value = "/findPublications/pubblicationName/{pubblicationName}", 
            method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> getPublicationsByName(@PathVariable String pubblicationName) {
        List<Pubblication> dbPublicationList = publicationService.findPublicationsByComicName(pubblicationName);
        return dbPublicationList;
    }
    
    @RequestMapping(value = "/findPublication/pubblicationName/{pubblicationName}/ordinal/{ordinal}", 
            method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Pubblication getPublicationsByNameAndOrdinal(@PathVariable String pubblicationName, @PathVariable int ordinal) {
        return publicationService.findPubblicationByOrdinalAndComicName(ordinal, pubblicationName);        
    }
    
    @RequestMapping(value = "/findLastPublication/pubblicationName/{pubblicationName}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Pubblication getLastPublicationByName(@PathVariable String pubblicationName) {
        Pubblication pub = publicationService.findLastPubblicationByComicName(pubblicationName);
        return pub;
    }
    
    

    @RequestMapping(value = "/loadNextPublications", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> loadNextPublications() {
        try {            
            List<Pubblication> newPubblicationListFromDb = publicationService.findNextPublications(Date.from(Instant.now().minus(1, ChronoUnit.DAYS)));
            Document doc = Jsoup.connect(nextPublicationsURL).timeout(0).get();
            List<Pubblication> newPubblicationListFromWeb
                    = PubblicationUtils.getPubblicationList(publicationService, authorService, doc, "newPubblication", shopBaseUrl);
            
            PubblicationUpdates pu = getPubblicationUpdates(newPubblicationListFromDb, newPubblicationListFromWeb);
            for (Pubblication p : pu.getNewPubblication()) {
                try {
                    publicationService.save(p);
                } catch (DataIntegrityViolationException ex) {                    
                    System.out.println("Exception ".concat(ex.getMessage()));
                    System.out.println("Duplicate pubblication ".concat(p.getPubblicationName()).concat(String.valueOf(p.getNumber())));
                }
            }
            for (Pubblication p : pu.getOldPubblication()) { 
                p.setCurrentInNewsstand(true);
                publicationService.update(p);                
            }
            return pu.getNewPubblication();
        } catch (IOException ex) {
            Logger.getLogger(PubblicationController.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>(1);
        }
//        return new ArrayList<>(1);
    }

    @RequestMapping(value = "/loadCurrentPublications", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> loadCurrentPublications() {
        try {
            Document doc = Jsoup.connect(currentPublicationsURL).timeout(0).get();
            List<Pubblication> currentInNewsstandFromDB = publicationService.findPublicationsCurrentInNewsstand();
            List<Pubblication> newPubblicationListFromWeb
                    = PubblicationUtils.getPubblicationList(publicationService, authorService, doc, "currentPubblication", shopBaseUrl);
            
            PubblicationUpdates pu = getPubblicationUpdates(currentInNewsstandFromDB, newPubblicationListFromWeb);
            for (Pubblication p : pu.getNewPubblication()) {
                try {       
                    p.setCurrentInNewsstand(true);
                    publicationService.save(p);
                } catch (DataIntegrityViolationException ex) {
                    System.out.println("Duplicate pubblication ".concat(p.getPubblicationName()).concat(String.valueOf(p.getNumber())));
                }
            }
            for (Pubblication p : pu.getOldPubblication()) { 
                p.setCurrentInNewsstand(false);
                publicationService.update(p);                
            }
                        
            return pu.getNewPubblication();
        } catch (IOException ex) {
            Logger.getLogger(PubblicationController.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>(1);
        }
//        return new ArrayList<>(1);
    }

    private PubblicationUpdates getPubblicationUpdates(List<Pubblication> pubblicationFromDb, List<Pubblication> pubblicationFromWeb) {
        List<Pubblication> newPub = new ArrayList<>(pubblicationFromWeb.size());
        List<Pubblication> oldPub = new ArrayList<>(pubblicationFromWeb.size());
        
        for(Pubblication p : pubblicationFromWeb) {
            boolean found = false;
            for(Pubblication p2 : pubblicationFromDb) {
                if(p2.equals(p)) {
                    found = true;                    
                    break;
                }
            }
            if(!found) {
                newPub.add(p);            
            }
        }    
        for (Pubblication p : pubblicationFromDb) {
            boolean found = false;
            for (Pubblication p2 : pubblicationFromWeb) {
                if (p2.equals(p)) {
                    found = true;
                    break;
                }
            }
            if (!found) {                
                oldPub.add(p);
            }
        }
        return new PubblicationUpdates(newPub, oldPub);
    }
    
    @RequestMapping(value = "/loadOldPublications/{pubblicationName}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Pubblication> loadOldPublications(@PathVariable String pubblicationName) {
        List<Pubblication> oldPublicationList = new LinkedList<>();

//        String pubblicationUrl = getPublicationUrl(pubblicationName);
//        if ("".equals(pubblicationUrl)) {
//            return oldPublicationList;
//        }
//        Pubblication oldPublication
//                = PubblicationUtils.getPubblication(publicationService, authorService, pubblicationUrl, "oldPublication");
//        if (oldPublication == null) {
//            return oldPublicationList;
//        }
//        try {
//            publicationService.save(oldPublication);
//            oldPublicationList.add(oldPublication);
//        } catch (DataIntegrityViolationException ex) {
//            System.out.println("Exception ".concat(ex.getLocalizedMessage()));
//            ex.printStackTrace();
//        }
//        while (!"".equals(oldPublication.getPreviousPubblicationURL())) {
//            oldPublication
//                    = PubblicationUtils.getPubblication(publicationService, authorService, oldPublication.getPreviousPubblicationURL(), "oldPublication");
//            try {
//                publicationService.save(oldPublication);
//                oldPublicationList.add(oldPublication);
//            } catch (DataIntegrityViolationException ex) {
//                System.out.println("Exception ".concat(ex.getLocalizedMessage()));
//                ex.printStackTrace();
//            }
//        }
        return oldPublicationList;
    }

    private String getPublicationUrl(String pubblicationName) {
        try {
            PublicationEnum pubblicationNameEnum = PublicationEnum.valueOf(pubblicationName);
            switch (pubblicationNameEnum) {
                case dylandog:
                    return dylanDogPublicationsURL;
                case colordylandog:
                    return colorDylanDogPublicationsURL;
                case colortex:
                    return colorTexUrl;
                case orfanisam:
                    return orfaniSamUrl;
                case julia:
                    return juliaUrl;
                case nathannever:
                    return nathanUrl;
                case zagor:
                    return zagorUrl;
                case tex:
                    return texUrl;
                case martinmystere:
                    return martinMystereUrl;
                default:
                    return "";
            }
        } catch (IllegalArgumentException ex) {
            return "";
        }
    }

    /*
    @RequestMapping(value = "/addPublication", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Publication addPublications() {
        Publication p = new Publication(new Date(), 0, shopBaseUrl, nextPublicationsURL, nextPublicationsURL, shopBaseUrl, colorTexUrl, shopBaseUrl, null, shopBaseUrl);
        p.setCurrentInNewsstand(true);
        pubblicationService.save(p);
        return p;
    }
    */
        
    @RequestMapping(value = "/getJsonAuthors", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<AuthorExport> getJsonAuthors() {
        List<Author> findAllAuthors = authorService.findAllAuthors();
        List<AuthorExport> authors = new ArrayList<>(findAllAuthors.size());
        
        for(Author a : findAllAuthors) {
            if(!"".equals(a.getNameSurname())) {
                authors.add(new AuthorExport(a.getNameSurname(), a.getName(), a.getSurname()));
            }
        }
        return authors;
    }
    
    @RequestMapping(value = "/getJsonTitles", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<AuthorExport> getJsonTitles() {
        List<Author> findAllAuthors = authorService.findAllAuthors();
        List<AuthorExport> authors = new ArrayList<>(findAllAuthors.size());
        
        for(Author a : findAllAuthors) {
            if(!"".equals(a.getNameSurname())) {
                authors.add(new AuthorExport(a.getNameSurname(), a.getName(), a.getSurname()));
            }
        }
        return authors;
    }
}
