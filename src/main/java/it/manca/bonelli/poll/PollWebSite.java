package it.manca.bonelli.poll;

import it.manca.bonelli.controller.PubblicationController;
import it.manca.bonelli.model.Pubblication;
import it.manca.bonelli.model.PubblicationUpdates;
import it.manca.bonelli.service.AuthorService;
import it.manca.bonelli.utils.PubblicationUtils;
import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import it.manca.bonelli.service.PublicationService;

/**
 *
 * @author Marco Manca
 */
@EnableAsync
public class PollWebSite {
       
    @Autowired
    private PublicationService publicationService;
    @Autowired
    private AuthorService authorService;
    @Autowired
    private String nextPublicationsURL;
    @Autowired
    private String currentPublicationsURL;
    @Autowired
    private String shopBaseUrl;
    
    public PollWebSite() {}

    @Async        
    public void loadPubblications() {        
        System.out.println("cron ".concat(Date.from(Instant.now()).toString()));
        loadCurrentPubblications();
        loadNextPubblications();
    }
    
    public List<Pubblication> loadCurrentPubblications() {
        try {
            Document doc = Jsoup.connect(currentPublicationsURL).timeout(0).get();
            List<Pubblication> currentInNewsstandFromDB = publicationService.findPublicationsCurrentInNewsstand();
            List<Pubblication> newPubblicationListFromWeb
                    = PubblicationUtils.getPubblicationList(publicationService, authorService, doc, "currentPubblication", shopBaseUrl);
            
            PubblicationUpdates pu = getPubblicationUpdates(currentInNewsstandFromDB, newPubblicationListFromWeb);
            for (Pubblication p : pu.getNewPubblication()) {
                try {       
                    p.setCurrentInNewsstand(true);
                    publicationService.save(p);
                } catch (DataIntegrityViolationException ex) {
                    System.out.println("Duplicate pubblication ".concat(p.getPubblicationName()).concat(String.valueOf(p.getNumber())));
                }
            }
            for (Pubblication p : pu.getOldPubblication()) { 
                p.setCurrentInNewsstand(false);
                publicationService.update(p);                
            }
                        
            return pu.getNewPubblication();
        } catch (IOException ex) {
            Logger.getLogger(PubblicationController.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>(1);
        }
    }
    
    private PubblicationUpdates getPubblicationUpdates(List<Pubblication> pubblicationFromDb, List<Pubblication> pubblicationFromWeb) {
        List<Pubblication> newPub = new ArrayList<>(pubblicationFromWeb.size());
        List<Pubblication> oldPub = new ArrayList<>(pubblicationFromWeb.size());
        
        for(Pubblication p : pubblicationFromWeb) {
            boolean found = false;
            for(Pubblication p2 : pubblicationFromDb) {
                if(p2.equals(p)) {
                    found = true;                    
                    break;
                }
            }
            if(!found) {
                newPub.add(p);            
            }
        }    
        for (Pubblication p : pubblicationFromDb) {
            boolean found = false;
            for (Pubblication p2 : pubblicationFromWeb) {
                if (p2.equals(p)) {
                    found = true;
                    break;
                }
            }
            if (!found) {                
                oldPub.add(p);
            }
        }
        return new PubblicationUpdates(newPub, oldPub);
    }
    
    public List<Pubblication> loadNextPubblications() {
        try {            
            List<Pubblication> newPubblicationListFromDb = publicationService.findNextPublications(Date.from(Instant.now().minus(1, ChronoUnit.DAYS)));
            Document doc = Jsoup.connect(nextPublicationsURL).timeout(0).get();
            List<Pubblication> newPubblicationListFromWeb
                    = PubblicationUtils.getPubblicationList(publicationService, authorService, doc, "newPubblication", shopBaseUrl);
            
            PubblicationUpdates pu = getPubblicationUpdates(newPubblicationListFromDb, newPubblicationListFromWeb);
            for (Pubblication p : pu.getNewPubblication()) {
                try {
                    publicationService.save(p);
                } catch (DataIntegrityViolationException ex) {                    
                    System.out.println("Exception ".concat(ex.getMessage()));
                    System.out.println("Duplicate pubblication ".concat(p.getPubblicationName()).concat(String.valueOf(p.getNumber())));
                }
            }
            for (Pubblication p : pu.getOldPubblication()) { 
                p.setCurrentInNewsstand(true);
                publicationService.update(p);                
            }
            return pu.getNewPubblication();
        } catch (IOException ex) {
            Logger.getLogger(PubblicationController.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>(1);
        }
    }
}
