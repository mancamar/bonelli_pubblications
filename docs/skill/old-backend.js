/* eslint-disable  func-names */
/* eslint-disable  no-console */

const Alexa = require('ask-sdk');
var https = require('https'); 
const skillBuilder = Alexa.SkillBuilders.standard();


function httpGet(param, callback) {
  let myAPI = {
    host: 'tare.isti.cnr.it',
    port: 443,
    path: '/BonelliPubblications/rest/countPubblicationsByAuthor/surname/'+param,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
};
myAPI.path = encodeURI(myAPI.path);
  console.log("requested started "+param)
  console.log("path "+myAPI.path);
  
  const req = https.request(myAPI, (res) => {
        res.setEncoding('utf8');
        console.log(`statusCode: ${res.statusCode}`)
        let returnData = '';

        res.on('data', (chunk) => {
            console.log("data", chunk);
            returnData += chunk;
        });
        res.on('end', () => {
            const retObj = JSON.parse(returnData)
            console.log("end", retObj);
            callback(retObj);
        });
    });
    
    req.on('error', (error) => {
      console.log("on error", error);
    })
  console.log("write data");
  //req.write()
  req.end()

  console.log("requested ended")
}

const LaunchRequestHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return request.type === 'LaunchRequest';  
  },
  handle(handlerInput) {
   const speechText = 'Benvenuto nel catalogo bonelli, cosa vuoi fare?';
  const repromptText = 'Prova a dire edicola o prossimamente';
    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt(repromptText)
      .withSimpleCard('Hello World', speechText)
      .getResponse();
  },
}

const SearchPubblicationsHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    console.log("request", request);
    return (request.type === 'IntentRequest'
        && request.intent.name === 'SearchPubblications');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    let speechOutput = "Ecco la pubblicazione ";

    let searchType = "no tipo";
    
    if (intentObj !== undefined && intentObj.slots !== undefined && intentObj.slots["search_type"] !== undefined) {
          searchType = intentObj.slots["search_type"].value;
    }
    
    let comicName = "no comic";
    if (intentObj !== undefined && intentObj.slots !== undefined && intentObj.slots["comic_name"] !== undefined) {
          comicName = intentObj.slots["comic_name"].value;
    }
    
    //const result = await httpGet();
    //const result = "Ecco i fumetti che cercavi!"
    return new Promise((resolve) => {
      console.log("Promise");
      httpGet((res) => {
        console.log("httpget called");
        const speechOutput = "return "+JSON.stringify(res);
                resolve(handlerInput.responseBuilder.speak(speechOutput).getResponse());
            });
    });
    /*
        console.log("speak", result);
        return handlerInput.responseBuilder
          .speak(result)
          .reprompt('test')
          .withSimpleCard(SKILL_NAME, 'test')
          .getResponse();
      */
    //speechOutput += "hai cercato " + searchType + " del fumetto "+comicName;
    
    
  }
};

const SearchPubblicationsByAuthorHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    console.log("request", request);
    return (request.type === 'IntentRequest'
        && request.intent.name === 'SearchPubblications');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    let speechOutput = "Ecco la pubblicazione ";

    let searchType = "no tipo";
    
    if (intentObj !== undefined && intentObj.slots !== undefined && intentObj.slots["search_type"] !== undefined) {
          searchType = intentObj.slots["search_type"].value;
    }
    
    let comicName = "no comic";
    if (intentObj !== undefined && intentObj.slots !== undefined && intentObj.slots["comic_name"] !== undefined) {
          comicName = intentObj.slots["comic_name"].value;
    }
    
    //const result = await httpGet();
    //const result = "Ecco i fumetti che cercavi!"
    return new Promise((resolve) => {
      console.log("Promise");
      httpGet((res) => {
        console.log("httpget called");
        const speechOutput = "return "+JSON.stringify(res);
                resolve(handlerInput.responseBuilder.speak(speechOutput).getResponse());
            });
    });
    /*
        console.log("speak", result);
        return handlerInput.responseBuilder
          .speak(result)
          .reprompt('test')
          .withSimpleCard(SKILL_NAME, 'test')
          .getResponse();
      */
    //speechOutput += "hai cercato " + searchType + " del fumetto "+comicName;
    
    
  }
};

const SearchPubblicationsByYearHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    console.log("request", request);
    return (request.type === 'IntentRequest'
        && request.intent.name === 'SearchPubblicationByYear');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    let speechOutput = "";

    let year = 2020;
    
    if (intentObj !== undefined && intentObj.slots !== undefined && intentObj.slots["year"] !== undefined) {
          year = intentObj.slots["year"].value;
    }
    
    let comicName = "no comic";
    if (intentObj !== undefined && intentObj.slots !== undefined && intentObj.slots["comic_name"] !== undefined) {
          comicName = intentObj.slots["comic_name"].value;
    }
    
    return new Promise((resolve) => {
      httpGet((res) => {        ;
        const speechOutput = "return "+JSON.stringify(res);
                resolve(handlerInput.responseBuilder.speak(speechOutput).getResponse());
            });
    });
    
  }
};

const SearchNextPubblicationsByComicName = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    console.log("request", request);
    return (request.type === 'IntentRequest'
        && request.intent.name === 'SearchNextPubblicationsByComicName' 
        && handlerInput.requestEnvelope.request.dialogState == 'COMPLETED');
  },
  handle(handlerInput) {
      return handlerInput.responseBuilder
      .speak('Richiesta ricevuta in modo completo ')
      .reprompt('Sorry, an error occurred.')
      .getResponse();
  }
};
const CountAuthorPubblicationHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    console.log("request", request);
    return (request.type === 'IntentRequest'
        && request.intent.name === 'CountAuthorPubblication');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    let speechOutput = "";

    let author = "";
    if (intentObj !== undefined && intentObj.slots !== undefined && intentObj.slots["author_name"] !== undefined) {
          if(intentObj.slots["author_name"].resolutions !== undefined &&
            intentObj.slots["author_name"].resolutions.resolutionsPerAuthority[0] !== undefined &&
            intentObj.slots["author_name"].resolutions.resolutionsPerAuthority[0].status.code === 'ER_SUCCESS_MATCH' &&
            intentObj.slots["author_name"].resolutions.resolutionsPerAuthority[0].values.length > 0) {
              author = intentObj.slots["author_name"].resolutions.resolutionsPerAuthority[0].values[0].value.name;  
              if(author == '') {
                author = intentObj.slots["author_name"].value;  
              }
            } else {
              author = intentObj.slots["author_name"].value;
            }
    }
    
    let comicName = "no comic";
    if (intentObj !== undefined && intentObj.slots !== undefined && intentObj.slots["comic_name"] !== undefined) {
          comicName = intentObj.slots["comic_name"].value;
    }
    
    return new Promise((resolve) => {
      httpGet(author, (res) => {
        const speechOutput = intentObj.slots["author_name"].value + " ha pubblicato "+res+" "+comicName;
        //const speechOutput = "return "+JSON.stringify(res);
                resolve(handlerInput.responseBuilder.speak(speechOutput).getResponse());
            });
    });
    
  }
};

const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    console.log(`Error handled: ${error.message}`);

    return handlerInput.responseBuilder
      .speak('Sorry, an error occurred: '+error.message)
      .reprompt('Sorry, an error occurred.')
      .getResponse();
  }
};

const SKILL_NAME = 'Pubblicazioni Bonelli';

exports.handler = skillBuilder
  .addRequestHandlers(
    LaunchRequestHandler,
    SearchPubblicationsHandler,
    SearchPubblicationsByAuthorHandler,
    SearchPubblicationsByYearHandler,
    CountAuthorPubblicationHandler,
    SearchNextPubblicationsByComicName
  )
  .addErrorHandlers(ErrorHandler)
  .lambda();

