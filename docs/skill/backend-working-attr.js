/* eslint-disable  func-names */
/* eslint-disable  no-console */

const Alexa = require('ask-sdk');
var https = require('https');
const skillBuilder = Alexa.SkillBuilders.standard();

function httpGet(requestPath, callback) {
  let myAPI = {
    host: 'tare.isti.cnr.it',
    port: 443,
    path: requestPath,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  };
  myAPI.path = encodeURI(myAPI.path);
  const req = https.request(myAPI, (res) => {
    res.setEncoding('utf8');
    let returnData = '';

    res.on('data', (chunk) => {
      returnData += chunk;
    });
    res.on('end', () => {
      const retObj = JSON.parse(returnData)
      callback(retObj);
    });
  });

  req.on('error', (error) => {
    console.log("on error", error);
  })
  req.end()
}

function getItalianRole(role) {
  switch (role) {
    case 'subject':
    case 'script':
      return "scritti";
    case 'illustration':
    case 'cover':
      return "disegnati";
    default:
      return undefined;
  }
}

function getItalianDate(date) {
  var dateSplit = date.split("-");
  return dateSplit[2]+"-"+dateSplit[1]+"-"+dateSplit[0];
}

function getSlotValue(slotName, intentObj) {
  if (intentObj !== undefined && intentObj.slots !== undefined && intentObj.slots[slotName] !== undefined) {
    if (slotName === 'comic_name') {
      var comicName = intentObj.slots["comic_name"].resolutions.resolutionsPerAuthority[0].values[0].value.id;
      if(comicName.includes("_")) {
        comicName = comicName.split("_").join(" ");
      }
      return comicName;
    } else {
      return intentObj.slots[slotName].value;
    }
  } else {
    return undefined;
  }
}

function getAuthor(intentObj) {
  //type nameSurname: se c'è trattino - si tratta di nameSurname. es. Luigi-Piccatto rimuovo trattino e lo sostiuisco con lo spazio
  //type surname:     se c'è underscore _ si tratta di spazio (surname) es: De_Tommaso rimuovo underscore e lo sostiuisco con lo spazio
  //type surname:     se non c'è nessuno dei due si tratta si surname
  let author = undefined;
  if (intentObj !== undefined && intentObj.slots !== undefined && intentObj.slots["author_name"] !== undefined) {
    if (intentObj.slots["author_name"].resolutions !== undefined &&
      intentObj.slots["author_name"].resolutions.resolutionsPerAuthority[0] !== undefined &&
      intentObj.slots["author_name"].resolutions.resolutionsPerAuthority[0].status.code === 'ER_SUCCESS_MATCH' &&
      intentObj.slots["author_name"].resolutions.resolutionsPerAuthority[0].values.length > 0) {
      author = intentObj.slots["author_name"].resolutions.resolutionsPerAuthority[0].values[0].value.name;
      if (author == '') {
        author = intentObj.slots["author_name"].value;
      }
    } else {
      author = intentObj.slots["author_name"].value;
    }
  }
  if (author.includes("-")) {
    return { id: author.replace("-", " "), type: 'authorNameSurname' };
  } else if (author.includes("_")) {
    return { id: author.replace("_", " "), type: 'authorSurname' };
  } else {
    return { id: author, type: 'authorSurname' };
  }

}

function getProsody(text) {
  return "<prosody pitch='medium' rate='medium'>" + text + "</prosody>";
}

function getBreak(type) {
  switch (type) {
    case 'short':
      return "<break time='250ms'/>";
    case 'medium':
      return "<break time='400ms'/>";
    case 'long':
      return "<break time='800ms'/>";
    default:
      return '';
  }
}

function sayAsDate(date) {
  return "<say-as interpret-as='date'>" + date + "</say-as>";
}

const LaunchRequestHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return request.type === 'LaunchRequest';
  },
  handle(handlerInput) {
    const speechText = 'Benvenuto nel catalogo bonelli, potrai trovare i fumetti ora in edicola, le prossime uscite, cercare gli arretrati o i fumetti di un autore. Cosa vuoi fare?';
    const repromptText = 'Prova a dire edicola per conoscere i fumetti in edicola, prossimamente per le prossime uscite, cerca per trovare gli arretrati o autore per i fumetti scritti o disegnati da un autore o autrice';
    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt(repromptText)
      //.withSimpleCard('Hello World', speechText)
      .getResponse();
  },
}

const CountPubByAuthorAndYear = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'CountPubByAuthorAndYear');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = intentObj.name;
    let author = getAuthor(intentObj);
    let year = getSlotValue("year", intentObj);
    if (author.id === undefined || year === undefined) {
      return handlerInput.responseBuilder
        .speak("Dimmi il nome dell'autore e l'anno di pubblicazione");
    }
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/countPublications/" + author.type + "/" + author.id + "/year/" + year;
      httpGet(path, (res) => {
        const speechOutput = getSlotValue("author_name", intentObj) + " ha pubblicato " + res + " fumetti durante l'anno " + year;
        resolve(handlerInput.responseBuilder.speak(speechOutput).getResponse());
      });
    });

  }
};

const CountPubByPubNameAndAuthor = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'CountPubByPubNameAndAuthor');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    let author = getAuthor(intentObj);
    let pubName = getSlotValue("comic_name", intentObj);
    if (author.id === undefined || pubName === undefined) {
      return handlerInput.responseBuilder
        .speak("Dimmi il nome dell'autore e il nome del fumetto");
    }
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/countPublications/pubblicationName/" + pubName + "/" + author.type + "/" + author.id;
      httpGet(path, (res) => {
        const speechOutput = getSlotValue("author_name", intentObj) + " ha pubblicato " + res + " " + pubName;
        resolve(handlerInput.responseBuilder.speak(speechOutput).getResponse());
      });
    });

  }
};

const CountPubByPubNameAndAuthorAndYear = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'CountPubByPubNameAndAuthorAndYear');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    let author = getAuthor(intentObj);
    let pubName = getSlotValue("comic_name", intentObj);
    let year = getSlotValue("year", intentObj);
    if (author.id === undefined || year === undefined || pubName === undefined) {
      return handlerInput.responseBuilder
        .speak("Dimmi il nome dell'autore e l'anno di pubblicazione e il nome del fumetto");
    }
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/countPublications/pubblicationName/" + pubName + "/" + author.type + "/" + author.id + "/year/" + year;
      httpGet(path, (res) => {
        const speechOutput = getSlotValue("author_name", intentObj) + " ha pubblicato " + res + " " + pubName + " durante l'anno " + year;
        resolve(handlerInput.responseBuilder.speak(speechOutput).getResponse());
      });
    });

  }
};

const FindPubInNewstand = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && (request.intent.name === 'FindPubInNewstand' || request.intent.name === 'FindPubAttrInNewstand'));
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    let searchField = undefined;
    const request = handlerInput.requestEnvelope.request;
    if (request.intent.name === 'FindPubAttrInNewstand') {
      searchField = getSlotValue("search_type", intentObj);
    }
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = intentObj.name;
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findPublicationInNewsstand";
      httpGet(path, (res) => {
        sessionAttributes.publications = [];
        let speechOutput = "Ecco i fumetti ora in edicola " + getBreak('short');
        if (request.intent.name === 'FindPubAttrInNewstand') {
          speechOutput = "Ecco il " + searchField + " dei fumetti ora in edicola " + getBreak('short');
          for (var i = 0; i < res.length; i++) {
            speechOutput += res[i][searchField];
          }
        } else {
          for (var i = 0; i < res.length; i++) {
            if (i < 7) {
              speechOutput += res[i].pubblicationName + " numero " + res[i].number + getBreak('short') + " dal titolo " + getProsody(res[i].pubblicationTitle) + getBreak('medium');
            } else {
              sessionAttributes.publications.push({ pubName: res[i].pubblicationName, pubNumber: res[i].number, pubTitle: res[i].pubblicationTitle })
            }
          }
          speechOutput += " Ci sono altri " + parseInt(res.length - 7) + " fumetti in elenco, vuoi che te li dica?" + getBreak('medium');
          console.log("speech out", speechOutput);
        }
        resolve(handlerInput.responseBuilder
          .speak(speechOutput)
          .reprompt("Vuoi sentire gli altri " + parseInt(res.length - 7) + " fumetti in edicola?")
          .getResponse());
      });
    });

  }
};

const FindPubInNewstandByAuthor = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'FindPubInNewstandByAuthor');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    let author = getAuthor(intentObj);
    if (author.id === undefined) {
      return handlerInput.responseBuilder
        .speak("Dimmi il nome dell'autore");
    }

    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findPublicationInNewsstand/" + author.type + "/" + author.id;
      httpGet(path, (res) => {
        let speechOutput = "";
        var role = "";
        for (var i = 0; i < res.length; i++) {
          for (var j = 0; j < res[i].pubblicationAuthors.length; j++) {
            if(res[i].pubblicationAuthors[j].pk.author.nameSurname === author.id ||
              res[i].pubblicationAuthors[j].pk.author.surname === author.id) {
              role = res[i].pubblicationAuthors[j].pk.role;       
            }
          }
          speechOutput += res[i].pubblicationName + " numero " + res[i].number + " dal titolo " + getProsody(res[i].pubblicationTitle) +getBreak('short')+" uscito il "+sayAsDate(res[i].pubblicationDate)+getBreak('short');
        }
        if(role !== undefined) {
          speechOutput = "Ecco i fumetti " + getItalianRole(role) + " da "+ getSlotValue("author_name", intentObj) + " ora in edicola "+getBreak('short') + speechOutput;
        } else {
          speechOutput = "Ecco i fumetti di "+ getSlotValue("author_name", intentObj) + " ora in edicola "+getBreak('short') + speechOutput;
        }
        resolve(handlerInput.responseBuilder.speak(speechOutput).getResponse());
      });
    });

  }
};

const FindPubInNewstandByPubName = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && (request.intent.name === 'FindPubInNewstandByPubName' || request.intent.name === 'FindPubAttrInNewstandByPubName'));
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const intentName = handlerInput.requestEnvelope.request.intent.name;
    let pubName = getSlotValue("comic_name", intentObj);
    let searchType = getSlotValue("search_type", intentObj);
    if (pubName === undefined) {
      return handlerInput.responseBuilder
        .speak("Dimmi il nome del fumetto");
    }

    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findPublicationInNewsstand/pubblicationName/" + pubName;
      httpGet(path, (res) => {
        let speechOutput = "Il " + pubName + " ora in edicola è il numero ";
        if (intentName === 'FindPubAttrInNewstandByPubName') {
          speechOutput = "Ecco il " + searchType + " dei fumetti di " + pubName + " ora in edicola "
          for (var i = 0; i < res.length; i++) {
            speechOutput += res[i].searchType;
          }
        } else {
          for (var i = 0; i < res.length; i++) {
            speechOutput += getProsody(res[i].number) + " dal titolo " + getBreak("short") + getProsody(res[i].pubblicationTitle) + getBreak('short') + " uscito il " + sayAsDate(res[i].pubblicationDate) + getBreak('short');
          }
        }
        console.log(speechOutput);
        resolve(handlerInput.responseBuilder.speak(speechOutput).getResponse());
      });
    });

  }
};

const FindPubInNewstandByTimeInterval = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'FindPubInNewstandByTimeInterval');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = intentObj.name;
    let timeInterval = getSlotValue("when_current", intentObj);
    if (timeInterval === undefined) {
      return handlerInput.responseBuilder
        .speak("Scegli settimana o mese");
    }

    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findPublicationInNewsstand/timeInterval/" + timeInterval;
      httpGet(path, (res) => {
        sessionAttributes.publications = [];
        var reprompt = "";
        let speechOutput = "Ecco i fumetti in edicola questa " + getProsody(timeInterval)+getBreak("short");
        var speakInterval = "questa settimana";
        sessionAttributes.speakInterval = speakInterval;
        if (timeInterval === "mese") {
          speechOutput = "Ecco i fumetti in edicola questo " + getProsody(timeInterval)+getBreak("short");
          speakInterval = "questo mese";
        }
        for (var i = 0; i < res.length; i++) {
          if (i < 7) {
            speechOutput += res[i].pubblicationName + " numero " + res[i].number + " dal titolo " + getProsody(res[i].pubblicationTitle)+getBreak("short");
            if(timeInterval === 'settimana') {
              speechOutput += " in uscita "+sayAsDate(res[i].pubblicationDate)+getBreak('short');
            }
          } else {
            sessionAttributes.publications.push({ pubName: res[i].pubblicationName, pubNumber: res[i].number, pubTitle: res[i].pubblicationTitle });
          }
        }
        if(res.length - 7 > 0) {
            speechOutput += getBreak("short")+" Ci sono altri " + parseInt(res.length - 7) + " fumetti in uscita "+speakInterval+", vuoi che te li dica?" + getBreak('medium');
            reprompt = "Vuoi sentire gli altri " + parseInt(res.length - 7) + " fumetti in uscita "+speakInterval+"?";
          }
          if(reprompt !== '') {
            resolve(handlerInput.responseBuilder
              .speak(speechOutput)
              .reprompt(reprompt)
              .getResponse());
          } else {
            resolve(handlerInput.responseBuilder.speak(speechOutput).getResponse());
          }
      });
    });

  }
};

const FindPubInNewstandByPubNameAndAuthor = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'FindPubInNewstandByPubNameAndAuthor');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    let pubName = getSlotValue("comic_name", intentObj);
    let author = getAuthor(intentObj);
    if (author.id === undefined || pubName === undefined) {
      return handlerInput.responseBuilder
        .speak("Dimmi il nome dell'autore e del fumetto");
    }

    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findPublicationInNewsstand/pubblicationName/" + pubName + "/" + author.type + "/" + author.id;
      httpGet(path, (res) => {
        let speechOutput = "";
        var role = undefined;
        for (var i = 0; i < res.length; i++) {
          for (var j = 0; j < res[i].pubblicationAuthors.length; j++) {
            if(res[i].pubblicationAuthors[j].pk.author.nameSurname === author.id ||
              res[i].pubblicationAuthors[j].pk.author.surname === author.id) {
              role = res[i].pubblicationAuthors[j].pk.role;       
            }
          }
          speechOutput += " è il numero " + res[i].number + " dal titolo " + getProsody(res[i].pubblicationTitle)+getBreak("short")+" uscito il "+sayAsDate(res[i].pubblicationDate)+getBreak("short");
        }
        if(role !== undefined) {
          speechOutput = "Il " + pubName + " " + getItalianRole(role) + " da " + getProsody(getSlotValue("author_name", intentObj)) + " ora in edicola "+getBreak('short') + speechOutput;
        } else {
          speechOutput = "Il " + pubName + " di " + getProsody(getSlotValue("author_name", intentObj)) + " ora in edicola "+getBreak('short') + speechOutput;
        }
        resolve(handlerInput.responseBuilder.speak(speechOutput).getResponse());
      });
    });
  }
};

const FindNextPub = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && (request.intent.name === 'FindNextPub' || request.intent.name === 'FindNextPubAttr'));
  },
  handle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = intentObj.name;
    let searchField = undefined;
    if (request.intent.name === 'FindNextPubAttr') {
      searchField = getSlotValue("search_type", intentObj);
    }
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findNextPublications";
      httpGet(path, (res) => {
        sessionAttributes.publications = [];
        let speechOutput = "Ecco i fumetti prossimamente in uscita "+getBreak("short");
        var reprompt = "";
        if (request.intent.name === 'FindNextPubAttr') {
          speechOutput = "Ecco il " + searchField + " dei fumetti prossimamente in uscita:";
          for (var i = 0; i < res.length; i++) {
            speechOutput += res[i][searchField];
          }
        } else {
          for (var i = 0; i < res.length; i++) {
            if (i < 7) {
              speechOutput += res[i].pubblicationName + " numero " + res[i].number + " dal titolo " + getProsody(res[i].pubblicationTitle)+getBreak("short");
            } else {
              sessionAttributes.publications.push({ pubName: res[i].pubblicationName, pubNumber: res[i].number, pubTitle: res[i].pubblicationTitle });
            }
          }
          if(res.length - 7 > 0) {
            speechOutput += getBreak("short")+" Ci sono altri " + parseInt(res.length - 7) + " fumetti in uscita prossimamente, vuoi che te li dica?" + getBreak('medium');
            reprompt = "Vuoi sentire gli altri " + parseInt(res.length - 7) + " fumetti in uscita prossimamente?";
            resolve(handlerInput.responseBuilder
              .speak(speechOutput)
              .reprompt(reprompt)
              .getResponse());
          } else {
            resolve(handlerInput.responseBuilder.speak(speechOutput).getResponse());   
          }
        }
      });
    });

  }
};

const FindNextPubByAuthor = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'FindNextPubByAuthor');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    let author = getAuthor(intentObj);
    if (author.id === undefined) {
      return handlerInput.responseBuilder
        .speak("Dimmi il nome dell'autore");
    }
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findNextPublications/" + author.type + "/" + author.id;
      httpGet(path, (res) => {
        let speechOutput = "";
        var role = undefined;
        for (var i = 0; i < res.length; i++) {
            for (var j = 0; j < res[i].pubblicationAuthors.length; j++) {
            if(res[i].pubblicationAuthors[j].pk.author.nameSurname === author.id ||
              res[i].pubblicationAuthors[j].pk.author.surname === author.id) {
              role = res[i].pubblicationAuthors[j].pk.role;       
            }
          }
          speechOutput += res[i].pubblicationName + " numero " + res[i].number + " dal titolo " + getProsody(res[i].pubblicationTitle)+", in uscita il "+sayAsDate(res[i].pubblicationDate)+getBreak("short");
        }
        if(role !== undefined) {
          speechOutput = "Ecco il fumetto " + getItalianRole(role) + " da " + getSlotValue("author_name", intentObj) + " prossimamente in uscita "+getBreak("short")+speechOutput;
        } else {
          speechOutput = "Ecco il fumetto di " + getSlotValue("author_name", intentObj) + " prossimamente in uscita "+getBreak("short")+speechOutput;
        }
        resolve(handlerInput.responseBuilder.speak(speechOutput).getResponse());
      });
    });

  }
};

const FindNextPubByPubName = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && (request.intent.name === 'FindNextPubByPubName' || request.intent.name === 'FindNextPubAttrByPubName'));
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const intentName = handlerInput.requestEnvelope.request.intent.name;
    let pubName = getSlotValue("comic_name", intentObj);
    if (pubName === undefined) {
      return handlerInput.responseBuilder
        .speak("Dimmi il nome del fumetto");
    }
    let searchType = getSlotValue("search_type", intentObj);
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findNextPublications/pubblicationName/" + pubName;
      httpGet(path, (res) => {
        let speechOutput = "Il numero di " + pubName + " prossimamente in uscita è il ";
        if (intentName === 'FindNextPubAttrByPubName') {
          speechOutput = "Ecco il " + searchType + " dei fumetti di " + pubName + " prossimamente in uscita:";
          for (var i = 0; i < res.length; i++) {
            speechOutput += res[i][searchType];
          }
        } else {
          for (var i = 0; i < res.length; i++) {
            speechOutput += getProsody(res[i].number) + " dal titolo " + getBreak("short") + getProsody(res[i].pubblicationTitle) + getBreak('medium') + " in uscita il " + getBreak('short') + sayAsDate(res[i].pubblicationDate);
          }
        }
        resolve(handlerInput.responseBuilder.speak(speechOutput).getResponse());
      });
    });

  }
};

const FindNextPubByPubNameAndAuthor = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'FindNextPubByPubNameAndAuthor');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    let author = getAuthor(intentObj);
    let pubName = getSlotValue("comic_name", intentObj);
    if (author.id === undefined || pubName === undefined) {
      return handlerInput.responseBuilder
        .speak("Dimmi il nome dell'autore e del fumetto");
    }
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findNextPublications/pubblicationName/" + pubName + "/" + author.type + "/" + author.id;
      httpGet(path, (res) => {
        let speechOutput = "";
        var role = undefined;
        for (var i = 0; i < res.length; i++) {
          for (var j = 0; j < res[i].pubblicationAuthors.length; j++) {
            if(res[i].pubblicationAuthors[j].pk.author.nameSurname === author.id ||
              res[i].pubblicationAuthors[j].pk.author.surname === author.id) {
              role = res[i].pubblicationAuthors[j].pk.role;       
            }
          }
          speechOutput += "numero " + res[i].number + " dal titolo " + res[i].pubblicationTitle+getBreak("short");
        }
        if(role !== undefined) {
          speechOutput = "Ecco il fumetto di " + pubName + " " + getItalianRole(role) + " da " + getSlotValue("author_name", intentObj) + " prossimamente in uscita "+getBreak("short")+speechOutput;
        } else {
          speechOutput = "Ecco il fumetto di " + pubName + " di " + getSlotValue("author_name", intentObj) + " prossimamente in uscita "+getBreak("short")+speechOutput;
        }
        resolve(handlerInput.responseBuilder.speak(speechOutput).getResponse());
      });
    });

  }
};

const FindPubByPubNameAndYear = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && (request.intent.name === 'FindPubByPubNameAndYear' || request.intent.name === 'FindPubAttrByPubNameAndYear'));
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const intentName = handlerInput.requestEnvelope.request.intent;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = intentObj.name;
    let pubName = getSlotValue("comic_name", intentObj);
    let year = getSlotValue("year", intentObj);
    sessionAttributes.pubName = pubName;
    sessionAttributes.year = year;
    if (pubName === undefined || year === undefined) {
      return handlerInput.responseBuilder
        .speak("Dimmi il nome del fumetto e l'anno");
    }
    let searchType = getSlotValue("search_type", intentObj);
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findPublications/pubblicationName/" + pubName + "/year/" + year;
      httpGet(path, (res) => {
        sessionAttributes.publications = [];
        var reprompt = "";
        let speechOutput = "Ecco i fumetti di " + pubName + " usciti nel " + year + getBreak("short");
        if (intentName === 'FindNextPubAttrByPubName') {
          speechOutput = "Ecco il " + searchType + " dei fumetti di " + pubName + " usciti nel " + year + ":";
          for (var i = 0; i < res.length; i++) {
            speechOutput += res[i].searchType;
          }
        } else {
          for (var i = 0; i < res.length; i++) {
            if (i < 7) {
              speechOutput += " numero " + res[i].number + " dal titolo " + getProsody(res[i].pubblicationTitle)+getBreak("short");
            } else {
              sessionAttributes.publications.push({ pubName: res[i].pubblicationName, pubNumber: res[i].number, pubTitle: res[i].pubblicationTitle });
            }
          }
        }
        if(res.length - 7 > 0) {
          speechOutput += getBreak("short")+" Ci sono altri " + parseInt(res.length - 7) + " "+pubName+" usciti nel "+year+", vuoi che te li dica?" + getBreak('medium');
          reprompt = "Vuoi sentire gli altri " + parseInt(res.length - 7) + " "+pubName+" usciti nel "+year+"?";
          resolve(handlerInput.responseBuilder
            .speak(speechOutput)
            .reprompt(reprompt)
            .getResponse());
        } else {
          resolve(handlerInput.responseBuilder.speak(speechOutput).getResponse());
        }
      });
    });

  }
};

const FindPubByAuthorAndYear = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && (request.intent.name === 'FindPubByAuthorAndYear' || request.intent.name === 'FindPubAttrByAuthorAndYear'));
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const intentName = handlerInput.requestEnvelope.request.intent.name;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = intentObj.name;
    let author = getAuthor(intentObj);
    let year = getSlotValue("year", intentObj);
    sessionAttributes.year = year;
    sessionAttributes.author = getSlotValue("author_name", intentObj);
    if (author.id === undefined || year === undefined) {
      return handlerInput.responseBuilder
        .speak("Dimmi il nome dell'autore e l'anno di pubblicazione");
    }
    let searchType = getSlotValue("search_type", intentObj);
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findPublications/" + author.type + "/" + author.id + "/year/" + year;
      httpGet(path, (res) => {
        sessionAttributes.publications = [];
        let speechOutput = "";
        var role = undefined;
        var reprompt = "";
        if (intentName === 'FindPubAttrByAuthorAndYear') {
          speechOutput = "Ecco il " + searchType + " dei fumetti di " + getSlotValue("author_name", intentObj) + " usciti nel " + year + ":";
          for (var i = 0; i < res.length; i++) {
            speechOutput += res[i][searchField];
          }
        } else {
          for (var i = 0; i < res.length; i++) {
            findAuthorRole :for (var j = 0; j < res[i].pubblicationAuthors.length; j++) {
              if(res[i].pubblicationAuthors[j].pk.author.nameSurname === author.id ||
                res[i].pubblicationAuthors[j].pk.author.surname === author.id) {
                role = res[i].pubblicationAuthors[j].pk.role;       
                break findAuthorRole; 
              }
            }
            if (i < 7) {
              speechOutput += res[i].pubblicationName + " numero " + res[i].number + " dal titolo " + getProsody(res[i].pubblicationTitle)+getBreak("short");
            } else {
              sessionAttributes.publications.push({ pubName: res[i].pubblicationName, pubNumber: res[i].number, pubTitle: res[i].pubblicationTitle });
            }
          }
          if(role !== undefined) {
            speechOutput = "Ecco i fumetti " + getItalianRole(role) + " da " + getSlotValue("author_name", intentObj) + " e usciti nel " + year + getBreak("short")+ speechOutput;
          } else {
            speechOutput = "Ecco i fumetti di " + getSlotValue("author_name", intentObj) + " e usciti nel " + year + getBreak("short")+ speechOutput;
          }
          if(res.length - 7 > 0) {
            speechOutput += getBreak("short")+" Ci sono altri " + parseInt(res.length - 7) + " fumetti pubblicati da "+getSlotValue("author_name", intentObj)+", vuoi che te li dica?" + getBreak('medium');
            reprompt = "Vuoi sentire gli altri " + parseInt(res.length - 7) + " fumetti pubblicati da "+getSlotValue("author_name", intentObj)+"?";
          }
        }
        if(reprompt !== '') {
          resolve(handlerInput.responseBuilder
            .speak(speechOutput)
            .reprompt(reprompt)
            .getResponse());
        } else {
          resolve(handlerInput.responseBuilder.speak(speechOutput).getResponse());
        }
      });
    });

  }
};

const FindPubByPubNameAndNumber = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && (request.intent.name === 'FindPubByPubNameAndNumber' || request.intent.name === 'FindPubAttrByPubNameAndNumber'));
  },
  handle(handlerInput) {
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const intentName = handlerInput.requestEnvelope.request.intent.name;
    sessionAttributes.previousIntent = intentName;
    let pubName = getSlotValue("comic_name", intentObj);
    let number = getSlotValue("comic_number", intentObj);
    if (pubName === undefined || number === undefined) {
      return handlerInput.responseBuilder
        .speak("Dimmi il nome del fumetto e il numero");
    }
    let searchType = getSlotValue("search_type", intentObj);
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findPublication/pubblicationName/" + pubName + "/pubblicationNumber/" + number;
      var card;
      var cardAuthorInfo = "";
      httpGet(path, (res) => {
        let speechOutput = "";
        if (intentName === 'FindNextPubAttrByPubName') {
          speechOutput = "Il " + searchType + " di " + pubName + " numero " + number + " è " + res.searchField;
        } else {
          speechOutput += "Il titolo del " + res.pubblicationName + " numero " + getProsody(res.number) + " è " + getBreak('short') + getProsody(res.pubblicationTitle) + getBreak('short');
          let authors = {
            subject: [],
            script: [],
            illustration: [],
            cover: []
          }
          for (var i = 0; i < res.pubblicationAuthors.length; i++) {
            authors[res.pubblicationAuthors[i].pk.role].push(res.pubblicationAuthors[i].pk.author.nameSurname)
          }
          if (areSameSubjectAndScript(authors.subject, authors.script)) {
            speechOutput += " soggetto e sceneggiatura di " + getProsody(getAuthorsFromRole(authors.subject))+getBreak('short');
            cardAuthorInfo += "soggetto e sceneggiatura di "+getAuthorsFromRole(authors.subject)+"\r\n";
          } else {
            speechOutput += " soggetto di " + getProsody(getAuthorsFromRole(authors.subject))+getBreak('short');
            cardAuthorInfo += " soggetto di "+getAuthorsFromRole(authors.subject)+"\r\n";
            speechOutput += " sceneggiatura di " + getProsody(getAuthorsFromRole(authors.script))+getBreak('short');
            cardAuthorInfo += " sceneggiatura di "+getAuthorsFromRole(authors.script)+"\r\n";
          }
          speechOutput += " disegni di " + getProsody(getAuthorsFromRole(authors.illustration))+getBreak('short');
          cardAuthorInfo += " disegni di "+getAuthorsFromRole(authors.illustration)+"\r\n";
          speechOutput += " copertina di " + getProsody(getAuthorsFromRole(authors.cover))+getBreak('short');
          cardAuthorInfo += " copertina di "+getAuthorsFromRole(authors.cover)+"\r\n";
          speechOutput += " uscito il "+sayAsDate(res.pubblicationDate)+getBreak('short');
          cardAuthorInfo += " uscito il "+getItalianDate(res.pubblicationDate)+"\r\n";
          res.summary = res.summary.toLowerCase();
          if (res.summary.startsWith("soggetto")) {
            var idxLabelCover = res.summary.indexOf("copertina:");
            var coverAuthor = authors.cover[0].toLowerCase();
            if (idxLabelCover !== -1) {
              console.log("cover author", coverAuthor);
              var idxCoverAuthor = res.summary.indexOf(coverAuthor, idxLabelCover);
              if (idxCoverAuthor !== -1) {
                console.log("summary", res.summary);
                var newSummary = res.summary.substring(idxCoverAuthor + coverAuthor.length, res.summary.length);
                res.summary = newSummary;
              }
            } else {
              var idxLabelCover = res.summary.indexOf("colori:");
              if (idxLabelCover !== -1) {
                var newSummary = res.summary.substring(idxLabelCover + 14, res.summary.length);
                res.summary = newSummary;
              }
            } 
          }
          speechOutput += " Vuoi sentire la sintesi della storia?" + getBreak('short');
          sessionAttributes.summary = res.summary;
          card = {
            "type": "Standard",
            "title": res.pubblicationName + " numero "+res.number+" titolo "+res.pubblicationTitle,
            "text": cardAuthorInfo+ "\r\n" + res.summary,
            "image": {
              "smallImageUrl": res.bigCoverURL,
              "largeImageUrl": res.bigCoverURL
            }
          };
        }
        resolve(handlerInput.responseBuilder
          .speak(speechOutput)
          .withStandardCard(card.title, card.text.substring(0, 500)+"[...]", card.image.smallCoverURL, card.image.largeImageUrl)
          //.withSimpleCard(card.title, card.text)
          .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
          .getResponse());
      });
    });

  }
};

function areSameSubjectAndScript(authorSubject, authorScript) {
  if (authorSubject.length === authorScript.length) {
    for (var i = 0; i < authorSubject.length; i++) {
      var found = false;
      subjectLabel: for (var j = 0; j < authorScript.length; j++) {
        if (authorSubject[i] === authorScript[j]) {
          found = true;
        }
        if (found) {
          break subjectLabel;
        }
      }
      if (!found) {
        return false;
      }
    }
  } else {
    return false;
  }
  return true;
}

function getAuthorsFromRole(authorsArray) {
  var toRet = "";
  if (authorsArray.length === 1) {
    return authorsArray[0];
  } else if (authorsArray.length === 2) {
    toRet += authorsArray[0] + ", ";
    toRet += authorsArray[1];
  } else {
    for (var i = 0; i < authorsArray.length - 1; i++) {
      toRet += authorsArray[i] + ", ";
    }
    toRet += authorsArray[authorsArray.length - 1];
  }
}

const FindPubByAuthor = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && (request.intent.name === 'FindPubByAuthor' || request.intent.name === 'FindPubAttrByAuthor'));
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const intentName = handlerInput.requestEnvelope.request.intent.name;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = intentObj.name;        
    sessionAttributes.author = getSlotValue("author_name", intentObj);
    let author = getAuthor(intentObj);
    if (author.id === undefined) {
      return handlerInput.responseBuilder
        .speak("Dimmi il nome dell'autore");
    }
    let searchType = getSlotValue("search_type", intentObj);
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findPublications/" + author.type + "/" + author.id;
      httpGet(path, (res) => {
        sessionAttributes.publications = [];
        let speechOutput = "";
        var role = undefined;
        var reprompt = "";
        if (intentName === 'FindPubAttrByAuthor') {
          speechOutput = "Ecco il " + searchType + " dei fumetti di " + getSlotValue("author_name", intentObj) + ":";
          for (var i = 0; i < res.length; i++) {
            speechOutput += res[i].searchField;
          }
        } else {
          
          for (var i = 0; i < res.length; i++) {
            labelAuthor :for (var j = 0; j < res[i].pubblicationAuthors.length; j++) {
              if(res[i].pubblicationAuthors[j].pk.author.nameSurname === author.id ||
                res[i].pubblicationAuthors[j].pk.author.surname === author.id) {
                role = res[i].pubblicationAuthors[j].pk.role;       
                break labelAuthor;
              }
            }
            if (i < 7) {
              speechOutput += res[i].pubblicationName + " numero " + res[i].number + " dal titolo " + getProsody(res[i].pubblicationTitle)+getBreak("short");
            } else {
              sessionAttributes.publications.push({ pubName: res[i].pubblicationName, pubNumber: res[i].number, pubTitle: res[i].pubblicationTitle })
            }
          }
          if(role !== undefined) {
            speechOutput = "I fumetti " + getItalianRole(role) + " da " + getSlotValue("author_name", intentObj) + "sono "+getBreak("short")+speechOutput;  
          } else {
            speechOutput = "I fumetti di " + getSlotValue("author_name", intentObj) + "sono "+getBreak("short")+speechOutput;  
          }
          if(parseInt(res.length - 7) > 0) {
            speechOutput += " Ci sono altri " + parseInt(res.length - 7) + " fumetti di "+getSlotValue("author_name", intentObj)+" in elenco, vuoi che te li dica?" + getBreak('medium');
            reprompt = " Vuoi sentire gli altri "+parseInt(res.length - 7)+ "fumetti di "+getSlotValue("author_name", intentObj)+"?";
          }          
        }
        if(reprompt === '') {
          resolve(handlerInput.responseBuilder.speak(speechOutput).getResponse());
        } else {
          resolve(handlerInput.responseBuilder
            .speak(speechOutput)
            .reprompt(reprompt)
            .getResponse());
        }
        
      });
    });

  }
};

const FindPubByAuthorAndPubName = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'FindPubByAuthorAndPubName');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = intentObj.name;
    let author = getAuthor(intentObj);
    let pubName = getSlotValue("comic_name", intentObj);
    
    sessionAttributes.author = getSlotValue("author_name", intentObj);
    sessionAttributes.pubName = pubName;
    
    if (author.id === undefined || pubName === undefined) {
      return handlerInput.responseBuilder
        .speak("Dimmi il nome dell'autore e del fumetto");
    }
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findPublications/" + author.type + "/" + author.id + "/pubblicationName/" + pubName;
      httpGet(path, (res) => {
        sessionAttributes.publications = [];
        var reprompt = "";
        let speechOutput = "";
        var role = undefined;
        for (var i = 0; i < res.length; i++) {
          for (var j = 0; j < res[i].pubblicationAuthors.length; j++) {
            if(res[i].pubblicationAuthors[j].pk.author.nameSurname === author.id ||
              res[i].pubblicationAuthors[j].pk.author.surname === author.id) {
              role = res[i].pubblicationAuthors[j].pk.role;       
            }
          }
          if(i < 7) {
            speechOutput += " il numero " + res[i].number + " dal titolo " + getProsody(res[i].pubblicationTitle)+getBreak("short");
          } else {
            sessionAttributes.publications.push({ pubName: res[i].pubblicationName, pubNumber: res[i].number, pubTitle: res[i].pubblicationTitle });
          }
        }
        if(role !== undefined) {
          speechOutput = "Il fumetti di " + pubName + " "+getItalianRole(role) + " da " + getProsody(getSlotValue("author_name", intentObj)) + "sono "+getBreak("short")+speechOutput;
        } else {
          speechOutput = "Il fumetti di " + pubName + " di " + getProsody(getSlotValue("author_name", intentObj)) + "sono "+getBreak("short")+speechOutput;
        }
        if(res.length - 7 > 0) {
          speechOutput += getBreak("short")+" Ci sono altri " + parseInt(res.length - 7) + " fumetti pubblicati da "+getSlotValue("author_name", intentObj)+", vuoi che te li dica?" + getBreak('medium');
          reprompt = "Vuoi sentire gli altri " + parseInt(res.length - 7) + " fumetti pubblicati da "+getSlotValue("author_name", intentObj)+"?";  
          resolve(handlerInput.responseBuilder
            .speak(speechOutput)
            .reprompt(reprompt)
            .getResponse());
        } else {
          resolve(handlerInput.responseBuilder.speak(speechOutput).getResponse());
        }
      });
    });

  }
};

const FindPubByAuthorAndPubNameAndYear = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'FindPubByAuthorAndPubNameAndYear');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = intentObj.name;
    
    let author = getAuthor(intentObj);
    let pubName = getSlotValue("comic_name", intentObj);
    let year = getSlotValue("year", intentObj);
    
    sessionAttributes.author = getSlotValue("author_name", intentObj)
    sessionAttributes.pubName = pubName;
    sessionAttributes.year = year;
    
    if (author.id === undefined || pubName === undefined
      || year === undefined) {
      return handlerInput.responseBuilder
        .speak("Dimmi il nome dell'autore, del fumetto e l'anno");
    }
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findPublications/" + author.type + "/" + author.id + "/pubblicationName/" + pubName + "/year/" + year;
      var reprompt = "";
      httpGet(path, (res) => {
        sessionAttributes.publications = [];
        let speechOutput = "";
        var role = undefined;
        for (var i = 0; i < res.length; i++) {
          for (var j = 0; j < res[i].pubblicationAuthors.length; j++) {
            authorLabel : if(res[i].pubblicationAuthors[j].pk.author.nameSurname === author.id ||
              res[i].pubblicationAuthors[j].pk.author.surname === author.id) {
              role = res[i].pubblicationAuthors[j].pk.role;       
              break authorLabel;
            }
          }
          if (i < 7) {
            speechOutput += " il numero " + res[i].number + " dal titolo " + getProsody(res[i].pubblicationTitle)+getBreak("short");
          } else {
            sessionAttributes.publications.push({ pubName: res[i].pubblicationName, pubNumber: res[i].number, pubTitle: res[i].pubblicationTitle });
          }
        }
        console.log("role", role);
        if(role !== undefined) {
          speechOutput = "Il fumetti di " + pubName + " " + getItalianRole(role) + " da "+ getSlotValue("author_name", intentObj) + " usciti nell'anno " + year + " sono "+getBreak("short") + speechOutput;
        } else {
          speechOutput = "Il fumetti di " + pubName + " di " + getSlotValue("author_name", intentObj) + " usciti nell'anno " + year + " sono "+getBreak("short") + speechOutput;
        }
        if(res.length - 7 > 0) {
            speechOutput += getBreak("short")+" Ci sono altri " + parseInt(res.length - 7) + " fumetti di "+getSlotValue("author_name", intentObj)+" usciti nel "+year+", vuoi che te li dica?" + getBreak('medium'); 
            reprompt = "Vuoi sentire gli altri " + parseInt(res.length - 7) + " fumetti pubblicati da "+getSlotValue("author_name", intentObj)+"nel "+year+"?";
        }
        if(reprompt === '') {
          resolve(handlerInput.responseBuilder.speak(speechOutput).getResponse());
        } else {
          resolve(handlerInput.responseBuilder
            .speak(speechOutput)
            .reprompt(reprompt)
            .getResponse());
        }
      });
    });

  }
};

const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    return handlerInput.responseBuilder
      .speak('Mi dispiace, si è verificato un errore ' + error.message)
      .getResponse();
  }
};

const StopIntentHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest' &&
      (request.intent.name === 'AMAZON.StopIntent' || request.intent.name === 'AMAZON.NoIntent'))
  },
  handle(handlerInput) {
    return handlerInput.responseBuilder
      .speak('Ciao, a presto!')
      .getResponse();
  }
};

const YesIntentHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'AMAZON.YesIntent');
  },
  handle(handlerInput) {
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    var speechOutput = "";
    var reprompt = "";
    switch (sessionAttributes.previousIntent) {
      case 'FindPubInNewstand':
      case 'FindPubByAuthorAndYear':
      case 'FindPubByAuthor':
      case 'FindPubByAuthorAndPubName':
      case 'FindPubByAuthorAndPubNameAndYear':
      case 'FindPubInNewstandByTimeInterval':
      case 'FindNextPub':
      case 'FindPubByPubNameAndYear':
        var newMsgArray = new Array();        
        for (var i = 0; i < sessionAttributes.publications.length; i++) {
          if (i < 7) {
            speechOutput += sessionAttributes.publications[i].pubName + " numero " + 
                sessionAttributes.publications[i].pubNumber + getBreak('short') +
                " dal titolo " + getProsody(sessionAttributes.publications[i].pubTitle) + getBreak('medium');
          } else {
            newMsgArray.push({ pubName: sessionAttributes.publications[i].pubName, 
                               pubNumber: sessionAttributes.publications[i].pubNumber, 
                               pubTitle: sessionAttributes.publications[i].pubTitle 
                            });
          }
        }
        sessionAttributes.publications = newMsgArray;
        if (sessionAttributes.publications.length > 0) {  
          if(sessionAttributes.previousIntent === 'FindPubInNewstand') {        
            speechOutput += getBreak("medium") + " Ci sono altri " + sessionAttributes.publications.length + " fumetti in edicola, vuoi che te li dica?" + getBreak('medium');
            reprompt = "Vuoi sentire gli altri " + parseInt(sessionAttributes.publications.length - (7)) + " fumetti in edicola?";
          } else if(sessionAttributes.previousIntent === 'FindPubByAuthorAndYear') {        
            speechOutput += getBreak("medium") + " Ci sono altri " + sessionAttributes.publications.length + " pubblicati da "+sessionAttributes.author+" nel "+sessionAttributes.year+", vuoi che te li dica?" + getBreak('medium');
            reprompt = "Vuoi sentire gli altri " + parseInt(sessionAttributes.publications.length - (7)) + " fumetti pubblicati da "+sessionAttributes.author+" nel "+sessionAttributes.year+"?";
          } else if(sessionAttributes.previousIntent === 'FindPubByAuthor') {        
            speechOutput += getBreak("medium") + " Ci sono altri " + sessionAttributes.publications.length + " pubblicati da "+sessionAttributes.author+", vuoi che te li dica?" + getBreak('medium');
            reprompt = "Vuoi sentire gli altri " + parseInt(sessionAttributes.publications.length - (7)) + " fumetti pubblicati da "+sessionAttributes.author+" ?";
          } else if(sessionAttributes.previousIntent === 'FindPubByAuthorAndPubName') {        
            speechOutput += getBreak("medium") + " Ci sono altri " + sessionAttributes.publications.length + " "+sessionAttributes.pubName+" pubblicati da "+sessionAttributes.author+", vuoi che te li dica?" + getBreak('medium');
            reprompt = "Vuoi sentire gli altri " + parseInt(sessionAttributes.publications.length - (7)) + " "+sessionAttributes.pubName+" pubblicati da "+sessionAttributes.author+" ?";
          } else if(sessionAttributes.previousIntent === 'FindPubByAuthorAndPubNameAndYear') {        
            speechOutput += getBreak("medium") + " Ci sono altri " + sessionAttributes.publications.length + " "+sessionAttributes.pubName+" pubblicati da "+sessionAttributes.author+" nel "+sessionAttributes.year+", vuoi che te li dica?" + getBreak('medium');
            reprompt = "Vuoi sentire gli altri " + parseInt(sessionAttributes.publications.length - (7)) + " fumetti pubblicati da "+sessionAttributes.author+" ?";
          } else if(sessionAttributes.previousIntent === 'FindPubInNewstandByTimeInterval') {        
            speechOutput += getBreak("medium") + " Ci sono altri " + sessionAttributes.publications.length + " fumetti in uscita "+sessionAttributes.speakInterval+", vuoi che te li dica?" + getBreak('medium');
            reprompt = "Vuoi sentire gli altri " + parseInt(sessionAttributes.publications.length - (7)) + " fumetti in uscita "+sessionAttributes.speakInterval+" ?";
          } else if(sessionAttributes.previousIntent === 'FindNextPub') {        
            speechOutput += getBreak("medium") + " Ci sono altri " + sessionAttributes.publications.length + " fumetti in uscita prossimamente, vuoi che te li dica?" + getBreak('medium');
            reprompt = "Vuoi sentire gli altri " + parseInt(sessionAttributes.publications.length - (7)) + " fumetti in uscita prossimamente?";
          } else if(sessionAttributes.previousIntent === 'FindPubByPubNameAndYear') {        
            speechOutput += getBreak("short")+" Ci sono altri " + parseInt(sessionAttributes.publications.length - 7) + " "+sessionAttributes.pubName+" usciti nel "+sessionAttributes.year+", vuoi che te li dica?" + getBreak('medium');
            reprompt = "Vuoi sentire gli altri " + parseInt(sessionAttributes.publications.length - (7)) + " "+sessionAttributes.pubName+" usciti nel "+sessionAttributes.year+"?";
          }
        }
        break;
      case 'FindPubByPubNameAndNumber':
        speechOutput = "Ecco la sintesi della storia "+getBreak('medium')+sessionAttributes.summary;
        break;
      default:
        speechOutput = "L'intent precedente era " + sessionAttributes.previousIntent;
    }
    if (reprompt !== '') {
      return handlerInput.responseBuilder
        .speak(speechOutput)
        .reprompt(reprompt)
        .getResponse();
    } else {
      return handlerInput.responseBuilder
        .speak(speechOutput)
        .getResponse();
    }
  }
};

const SKILL_NAME = 'Catalogo Bonelli';

exports.handler = skillBuilder
  .addRequestHandlers(
    LaunchRequestHandler,
    StopIntentHandler,
    YesIntentHandler,
    CountPubByAuthorAndYear,
    CountPubByPubNameAndAuthor,
    CountPubByPubNameAndAuthorAndYear,
    FindPubByPubNameAndYear, //FindPubAttrByPubNameAndYear,
    FindPubByPubNameAndNumber, //FindPubAttrByPubNameAndNumber,
    FindPubByAuthor, //FindPubAttrByAuthor,
    FindPubByAuthorAndYear, //FindPubAttrByAuthorAndYear,
    FindPubByAuthorAndPubNameAndYear,
    FindPubByAuthorAndPubName,
    FindPubInNewstand,//FindPubAttrInNewstand,
    FindPubInNewstandByTimeInterval,
    FindPubInNewstandByPubName,//FindPubAttrInNewstandByPubName,
    FindPubInNewstandByAuthor,
    FindPubInNewstandByPubNameAndAuthor,
    FindNextPub, //FindNextPubAttr
    FindNextPubByAuthor,
    FindNextPubByPubNameAndAuthor,
    FindNextPubByPubName //FindNextPubAttrByPubName,    
  )
  .addErrorHandlers(ErrorHandler)
  .lambda();

