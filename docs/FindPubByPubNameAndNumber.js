const FindPubByPubNameAndNumber = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && (request.intent.name === 'FindPubByPubNameAndNumber'));
  },
  handle(handlerInput) {
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const intentName = handlerInput.requestEnvelope.request.intent.name;
    sessionAttributes.previousIntent = intentName;
    let pubName = getSlotValue("comic_name", intentObj);
    let number = getSlotValue("comic_number", intentObj);
    if (pubName === undefined || number === undefined) {
      return handlerInput.responseBuilder
        .speak("Dimmi il nome del fumetto e il numero");
    }
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findPublication/pubblicationName/" + pubName + "/pubblicationNumber/" + number;
      var cardAuthorInfo = "";
      httpGet(path, (res) => {
        let speechOutput = "";

        let authors = {
          subject: [],
          script: [],
          illustration: [],
          cover: []
        }
        if (res.number !== -1) {
          speechOutput += "Il titolo del " + res.pubblicationName + " numero " + getProsody(res.number) + " è " + getBreak('short') + getProsody(res.pubblicationTitle) + getBreak('short');
          for (var i = 0; i < res.pubblicationAuthors.length; i++) {
            authors[res.pubblicationAuthors[i].pk.role].push(res.pubblicationAuthors[i].pk.author.nameSurname)
          }
          if (areSameSubjectAndScript(authors.subject, authors.script)) {
            speechOutput += " soggetto e sceneggiatura di " + getProsody(getAuthorsFromRole(authors.subject)) + getBreak('short');
            cardAuthorInfo += "Soggetto e sceneggiatura di " + getAuthorsFromRole(authors.subject) + "<br>";
          } else {
            speechOutput += " soggetto di " + getProsody(getAuthorsFromRole(authors.subject)) + getBreak('short');
            cardAuthorInfo += "Soggetto di " + getAuthorsFromRole(authors.subject) + "<br>";
            speechOutput += " sceneggiatura di " + getProsody(getAuthorsFromRole(authors.script)) + getBreak('short');
            cardAuthorInfo += " Sceneggiatura di " + getAuthorsFromRole(authors.script) + "<br>";
          }
          speechOutput += " disegni di " + getProsody(getAuthorsFromRole(authors.illustration)) + getBreak('short');
          cardAuthorInfo += "Disegni di " + getAuthorsFromRole(authors.illustration) + "<br>";
          speechOutput += " copertina di " + getProsody(getAuthorsFromRole(authors.cover)) + getBreak('short');
          cardAuthorInfo += "Copertina di " + getAuthorsFromRole(authors.cover) + "<br>";
          speechOutput += " uscito il " + sayAsDate(res.pubblicationDate) + getBreak('short');
          //cardAuthorInfo += " uscito il " + getItalianDate(res.pubblicationDate) + "<br>";
          res.summary = res.summary.toLowerCase();
          if (res.summary.startsWith("soggetto")) {
            var idxLabelCover = res.summary.indexOf("copertina:");
            var coverAuthor = authors.cover[0].toLowerCase();
            if (idxLabelCover !== -1) {
              var idxCoverAuthor = res.summary.indexOf(coverAuthor, idxLabelCover);
              if (idxCoverAuthor !== -1) {
                console.log("summary", res.summary);
                var newSummary = res.summary.substring(idxCoverAuthor + coverAuthor.length, res.summary.length);
                res.summary = newSummary;
              }
            } else {
              var idxLabelCover = res.summary.indexOf("colori:");
              if (idxLabelCover !== -1) {
                var newSummary = res.summary.substring(idxLabelCover + 14, res.summary.length);
                res.summary = newSummary;
              }
            }
          }
          speechOutput += " Vuoi sentire la sintesi della storia?" + getBreak('short');
          sessionAttributes.summary = res.summary;

          resolve(handlerInput.responseBuilder
            .speak(speechOutput)
            /*.withStandardCard(res.pubblicationName + " numero " + res.number + " titolo " + res.pubblicationTitle,
              cardAuthorInfo + "\r\n" + res.summary,
              res.bigCoverURL, res.bigCoverURL)*/
            .addDirective({
              type: 'Alexa.Presentation.APL.RenderDocument',
              document: require('./apl/single_publication.json'),
              datasources: createSingleDataSource(res, cardAuthorInfo)
            })
            .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
            .getResponse());
        } else {
          speechOutput = "Non ho trovato il " + pubName + " numero " + number;
          resolve(handlerInput.responseBuilder
            .speak(speechOutput)
            .getResponse());
        }
      });
    });

  }
};
