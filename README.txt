creare il db con chartset default (utf8)
create le tabelle
modificare charset al db
ALTER SCHEMA `bonelli`  DEFAULT CHARACTER SET utf8mb4  DEFAULT COLLATE utf8mb4_unicode_ci ;

modificare il charset per la tabella pubblication impostando utf8mb4_unicode_ci

ALTER TABLE `bonelli`.`pubblication` 
CHARACTER SET = utf8mb4 , COLLATE = utf8mb4_unicode_ci ;

ALTER TABLE bonelli.pubblication modify bonelli.pubblication.summary longtext  
    CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci		
		
ALTER TABLE `bonelli`.`author` 
CHARACTER SET = utf8mb4 , COLLATE = utf8mb4_unicode_ci ;

ALTER TABLE `bonelli`.pubblication_author
CHARACTER SET = utf8mb4 , COLLATE = utf8mb4_unicode_ci ;

ALTER TABLE `bonelli`.pubblication_tag
CHARACTER SET = utf8mb4 , COLLATE = utf8mb4_unicode_ci ;

ALTER TABLE `bonelli`.tag
CHARACTER SET = utf8mb4 , COLLATE = utf8mb4_unicode_ci ;


drop table if exists Author
drop table if exists hibernate_sequence
drop table if exists Pubblication
drop table if exists pubblication_author
drop table if exists pubblication_tag
drop table if exists Tag

create table hibernate_sequence (next_val bigint) engine=MyISAM
insert into hibernate_sequence values ( 1 )

create table Author (authorId bigint not null, name varchar(255), nameSurname varchar(255), surname varchar(255), primary key (authorId)) engine=MyISAM

create table Pubblication (number integer not null, pubblicationName varchar(191) not null, bigCoverURL varchar(255) not null, currentInNewsstand bit not null, periodicity varchar(255) not null, pubblicationDate date not null, pubblicationTitle varchar(255) not null, schedaURL varchar(255) not null, smallCoverURL varchar(255) not null, summary LONGTEXT, primary key (number, pubblicationName)) engine=MyISAM;

create table pubblication_author (role integer, authorId bigint not null, number integer not null, pubblicationName varchar(191) not null, primary key (role, authorId, number, pubblicationName)) engine=MyISAM;

create table Tag (tagName varchar(191) not null, tagLink varchar(255), primary key (tagName)) engine=MyISAM

create table pubblication_tag (number integer not null, pubblicationName varchar(191) not null, tagName varchar(191) not null) engine=MyISAM

alter table pubblication_author add constraint FK7vpqrvrulvuyashf3jbjot0v5 foreign key (authorId) references Author (authorId);
alter table pubblication_author add constraint FKs0rqmt26ntavc5h6atebyvihv foreign key (number, pubblicationName) references Pubblication (number, pubblicationName);

alter table pubblication_tag add constraint FKlp2g49f47ot9e6e0o3sd6ywoq foreign key (tagName) references Tag (tagName);
alter table pubblication_tag add constraint FKau3yy6uae15bcixt5tkwerqoa foreign key (number, pubblicationName) references Pubblication (number, pubblicationName);